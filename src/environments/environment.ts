// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:9090/api',
  authEndPoint: '/authentication',
  boardEndpoint: '/board',
  lobbyEndPoint: '/lobby',
  playerEndPoint: '/player',
  notificationSettingsEndPoint: '/notificationsettings',
  playerStatsEndpoint: '/stats',
  streamEndPoint: '/stream',
  gameEndPoint: '/game',
  replayEndPoint: '/replay',
  googleApiKey: '929438373271-1j4tkcpbkeg5b63okvt3mu993puis9ru.apps.googleusercontent.com',
  facebookApiKey: '766159310406307',
  firebase: {
    apiKey: 'AIzaSyC1EHKepRSb_Qols4eyqzjzufRCtXUgDEo',
    authDomain: 'operation-health-stratego.firebaseapp.com',
    databaseURL: 'https://operation-health-stratego.firebaseio.com',
    projectId: 'operation-health-stratego',
    storageBucket: 'operation-health-stratego.appspot.com',
    messagingSenderId: '173635331615'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
