export const environment = {
  production: true,
  apiUrl: 'http://78.20.35.13:9090/api',
  authEndPoint: '/authentication',
  boardEndpoint: '/board',
  lobbyEndPoint: '/lobby',
  playerEndPoint: '/player',
  playerStatsEndpoint: '/stats',
  streamEndPoint: '/stream',
  gameEndPoint: '/game',
  replayEndPoint: '/replay',
  notificationSettingsEndPoint: '/notificationsettings',
  googleApiKey: '929438373271-1j4tkcpbkeg5b63okvt3mu993puis9ru.apps.googleusercontent.com',
  facebookApiKey: '766159310406307',
  firebase: {
    apiKey: 'AIzaSyC1EHKepRSb_Qols4eyqzjzufRCtXUgDEo',
    authDomain: 'operation-health-stratego.firebaseapp.com',
    databaseURL: 'https://operation-health-stratego.firebaseio.com',
    projectId: 'operation-health-stratego',
    storageBucket: 'operation-health-stratego.appspot.com',
    messagingSenderId: '173635331615'
  }
};
