import GameRules from './gamerules';
import GamePlayer from './gameplayer';
import GameState from './gamestate';

export default class Game {

  type: string;
  gameState: GameState;
  gameRules: GameRules;
  gamePlayers: GamePlayer[];

}
