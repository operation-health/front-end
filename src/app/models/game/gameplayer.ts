import Team from './team';
import Pawn from './pawn';
import {Player} from '../player';
import {Difficulty} from '../lobby/difficulty';

export default class GamePlayer {

  team: Team;
  afk: boolean;
  dead: boolean;
  host: boolean;
  computer: boolean;
  difficulty: Difficulty;
  currentTurn: boolean;
  ready: boolean;
  pawns: Pawn[];
  player: Player;

  constructor(computer: boolean, difficulty: Difficulty) {
    this.computer = computer;
    this.difficulty = difficulty;
    this.ready = true
  }
}
