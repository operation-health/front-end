import Rank from '../board/rank';
import {BoardTile} from '../board/boardTile';

export default class Pawn {

  id: number;
  rank: Rank;
  dead: boolean;
  boardTile: BoardTile;


  constructor(id: number, rank: Rank, dead: boolean, boardTile: BoardTile) {
    this.id = id;
    this.rank = rank;
    this.dead = dead;
    this.boardTile = boardTile;
  }
}
