enum Team {
  TEAM1 = 'TEAM1',
  TEAM2 = 'TEAM2',
  TEAM3 = 'TEAM3',
  TEAM4 = 'TEAM4'
}

export default Team;
