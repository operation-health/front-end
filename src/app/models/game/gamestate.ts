enum GameState {
  LOBBY = 'LOBBY',
  PREPARING = 'PREPARING',
  PLAYING = 'PLAYING',
  FINISHED = 'FINISHED'
}

export default GameState;
