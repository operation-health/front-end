import ActionEvent from '../event/action-event';
import Game from '../game/game';

export class ReplayGame extends Game {
  events: ActionEvent[];
}
