export class ReplayListItem {
  id: number;
  boardType: string;
  endTime: string;
  victory: boolean;
}
