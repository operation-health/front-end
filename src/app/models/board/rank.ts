enum Rank {
  FLAG = 'FLAG',
  SPY = 'SPY',
  SCOUT = 'SCOUT',
  MINER = 'MINER',
  SERGEANT = 'SERGEANT',
  LIEUTENANT = 'LIEUTENANT',
  CAPTAIN = 'CAPTAIN',
  MAJOR = 'MAJOR',
  COLONEL = 'COLONEL',
  GENERAL = 'GENERAL',
  MARSHAL = 'MARSHAL',
  BOMB = 'BOMB'
}

const canMove = (rank: Rank): boolean => {
  switch (rank) {
    case Rank.BOMB:
    case Rank.FLAG:
      return false;
    default:
      return true;
  }
};

const order = {
  [Rank.FLAG]: 0,
  [Rank.BOMB]: 1,
  [Rank.SPY]: 2,
  [Rank.SCOUT]: 3,
  [Rank.MINER]: 4,
  [Rank.SERGEANT]: 5,
  [Rank.LIEUTENANT]: 6,
  [Rank.CAPTAIN]: 7,
  [Rank.MAJOR]: 8,
  [Rank.COLONEL]: 9,
  [Rank.GENERAL]: 10,
  [Rank.MARSHAL]: 11
};
const getOrder = (rank: Rank): number => {
  return order[rank];
};

export default Rank;
export {
  canMove,
  getOrder
};
