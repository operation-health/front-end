import {BoardTile} from './boardTile';

export class Board {
  type: string;
  boardTiles: BoardTile[];
}
