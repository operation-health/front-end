export interface Tab {
  label: string;
  type: TabType;
  component: object;
  open?: boolean;
  disabled?: boolean;
}

export enum TabType {
  FRIENDS = 'FRIENDS',
  CHAT_GAME = 'CHAT_GAME'
}
