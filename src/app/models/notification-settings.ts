export class NotificationSettings {
  gameInvite: boolean;
  message: boolean;
  yourTurn: boolean;
  turnPlayed: boolean;
  turnExpired: boolean;
}
