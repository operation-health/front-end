import GamePlayer from '../game/gameplayer';
import GameRules from '../game/gamerules';

export class Lobby {
  publicAccess: boolean;
  gameRules: GameRules;
  board: {
    type: string,
    playerCount: number
  };
  gameState: string;
  gamePlayers: GamePlayer[];
}
