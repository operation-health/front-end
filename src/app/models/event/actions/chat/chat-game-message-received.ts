import Payload from '../../payload';

export default class ChatGameMessageReceived implements Payload {

  playerName: string;
  date: Date;
  message: string;

}
