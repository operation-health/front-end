import Payload from '../../payload';

export default class ChatPrivateMessageReceived implements Payload {

  playerName: string;
  date: Date;
  message: string;

}
