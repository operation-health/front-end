import Payload from '../../payload';

export class LobbyPlayerReadyChanged implements Payload {
  playerName: string;
  ready: boolean;
}
