import Payload from '../../payload';
import Team from '../../../game/team';

export class LobbyAiLeft implements Payload {
  team: Team;
}
