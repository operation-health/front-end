import Payload from '../../payload';

export class LobbyTurnTimerChanged implements Payload {
  turnTimer: number;
}
