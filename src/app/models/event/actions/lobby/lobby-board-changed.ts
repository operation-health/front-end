import Payload from '../../payload';

export class LobbyBoardChanged implements Payload {
  board: {
    type: string,
    playerCount: number
  };
}
