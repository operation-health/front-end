import Payload from '../../payload';

export class LobbyAfkTimerChanged implements Payload {
  afkTimer: number;
}
