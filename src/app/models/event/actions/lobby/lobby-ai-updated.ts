import Payload from '../../payload';
import GamePlayer from '../../../game/gameplayer';

export class LobbyAiUpdated implements Payload {
  gamePlayer: GamePlayer;
}
