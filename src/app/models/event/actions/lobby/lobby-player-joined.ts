import GamePlayer from '../../../game/gameplayer';
import Payload from '../../payload';

export class LobbyPlayerJoined implements Payload {
  gamePlayer: GamePlayer;
}
