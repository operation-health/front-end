import Payload from '../../payload';

export class LobbyHostLeft implements Payload {
  hostPlayerName: string;
  newHostPlayerName: string;
}
