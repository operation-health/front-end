import Payload from '../../payload';

export class LobbyPlayerLeft implements Payload {
  playerName: string;
}
