import Payload from '../../payload';

export class LobbyAccessChanged implements Payload {
  access: boolean;
}
