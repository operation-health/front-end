import Payload from '../../payload';

export class LobbyPrepTimerChanged implements Payload {
  preparationTimer: number;
}
