import Payload from '../../payload';
import GamePlayer from '../../../game/gameplayer';

export class LobbyAiJoined implements Payload {
  gamePlayer: GamePlayer;
}
