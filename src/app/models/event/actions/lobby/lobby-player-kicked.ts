import Payload from '../../payload';

export class LobbyPlayerKicked implements Payload {
  playerName: string;
}
