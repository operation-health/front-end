import Payload from '../../payload';
import Team from '../../../game/team';

export default class GamePlayerAfk implements Payload {
  currentTeam: Team;
  nextTeam: Team;
}
