import Payload from '../../payload';

export default class GamePawnOrganised implements Payload {
  pawnId: number;
  tileId: number;
}
