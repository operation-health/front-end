import Payload from '../../payload';
import Team from '../../../game/team';
import Rank from '../../../board/rank';

export default class GamePawnAttacked implements Payload {
  movedPawnId: number;
  movedPawnRank: Rank;
  movedPawnDead: boolean;
  attackedPawnId: number;
  attackedPawnRank: Rank;
  attackedPawnDead: boolean;
  tileId: number;
  currentTeam: Team;
  nextTeam: Team;
  attackedTeam: Team;
}
