import Payload from '../../payload';
import Team from '../../../game/team';

export default class GamePlayerReadyChanged implements Payload {
  team: Team;
  ready: boolean;
}
