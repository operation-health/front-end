import Payload from '../../payload';
import Game from '../../../game/game';

export default class GameTransitionToPlaying implements Payload {
  game: Game;
}
