import Payload from '../../payload';
import Team from '../../../game/team';
import Rank from '../../../board/rank';

export default class GameAttritionKill implements Payload {
  movedPawnId: number;
  movedPawnRank: Rank;
  movedPawnDead: boolean;
  attackedPawnRank: Rank;
  attackedPawnDead: boolean;
  tileId: number;
  currentTeam: Team;
  currentTeamDead: boolean;
  attackedTeam: Team;
  nextTeam: Team;
}
