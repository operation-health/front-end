import Payload from '../../payload';
import Team from '../../../game/team';
import Rank from '../../../board/rank';

export default class GamePassiveWin implements Payload {
  movedPawnRank: Rank;
  movedPawnDead: boolean;
  attackedPawnRank: Rank;
  attackedPawnDead: boolean;
  currentTeam: Team;
  attackedTeam: Team;
}
