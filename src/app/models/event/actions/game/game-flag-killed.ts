import Team from '../../../game/team';
import Payload from '../../payload';
import Rank from '../../../board/rank';

export default class GameFlagKilled implements Payload {
  movedPawnId: number;
  movedPawnRank: Rank;
  movedPawnDead: boolean;
  attackedPawnRank: Rank;
  attackedPawnDead: boolean;
  tileId: number;
  currentTeam: Team;
  attackedTeam: Team;
  nextTeam: Team;
}
