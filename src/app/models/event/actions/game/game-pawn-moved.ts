import Payload from '../../payload';
import Team from '../../../game/team';

export default class GamePawnMoved implements Payload {
  pawnId: number;
  tileId: number;
  currentTeam: Team;
  nextTeam: Team;
}
