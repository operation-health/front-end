import Payload from '../../payload';
import Team from '../../../game/team';
import Rank from '../../../board/rank';

export default class GameHumansEliminated implements Payload {
  movedPawnId: number;
  movedPawnRank: Rank;
  movedPawnDead: boolean;
  attackedPawnRank: Rank;
  attackedPawnDead: boolean;
  tileId: number;
  currentTeam: Team;
  currentTeamDead: boolean;
  attackedTeam: Team;
  attackedTeamDead: boolean;
}
