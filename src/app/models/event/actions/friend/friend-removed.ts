import Payload from '../../payload';

export default class FriendRemoved implements Payload {

  playerName: string;

}
