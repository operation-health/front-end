import Payload from '../../payload';

export default class FriendRequest implements Payload {

  playerName: string;

}
