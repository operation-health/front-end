import Payload from '../../payload';

export default class FriendRequestAccepted implements Payload {

  playerName: string;

}
