import Action from './action';
import Payload from './payload';

export default class ActionEvent {
  action: Action;
  payload: Payload;
}
