export class PlayerStats {
  gamesPlayed: number;
  gamesWon: number;
}
