import {PlayerStats} from './player-stats';

export class ProfilePlayer {
  playerName: String;
  profilePicture: String;
  playerStats: PlayerStats;
}
