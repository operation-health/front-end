export class GameMessage {
  date?: Date;
  message: String;
  playerName?: String;
}
