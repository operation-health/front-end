export class User {
  playerName: string;
  email: string;
  password: string;
  profilePicture?: string;
}
