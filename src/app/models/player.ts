export class Player {
  id: number;
  lastOnline: Date;
  playerName: string;
  profilePicture: any;
}
