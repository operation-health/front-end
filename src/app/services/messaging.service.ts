import {Injectable} from '@angular/core';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {BehaviorSubject, Subscription} from 'rxjs'
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  currentMessage = new BehaviorSubject(null);
  private messageSubscription: Subscription;

  constructor(
    private angularFireMessaging: AngularFireMessaging, private http: HttpClient, private router: Router) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  updateToken(token) {
    this.http.patch(environment.apiUrl + environment.playerEndPoint + '/fcm/' + token, null)
      .subscribe();
  }

  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        this.updateToken(token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  receiveMessage() {
    this.messageSubscription = this.angularFireMessaging.messages.subscribe(
      (message: any) => {
        if (message.data.background === 'false') {
          const notificationOptions = {
            body: message.notification.body,
          };
          const notification = new Notification(message.notification.title, notificationOptions).addEventListener('notificationclick',
            () => {
              this.router.navigate([message.notification.data.FCM_MSG.data.url]);
              console.log(message.notification.data.FCM_MSG.data.url)
            });
          this.currentMessage.next(message);
        }
      }
    );
  }
}
