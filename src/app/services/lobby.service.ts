import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import UpdatePrivacy from '../models/lobby/update-privacy';
import UpdateBoard from '../models/lobby/update-board';
import {Lobby} from '../models/lobby/lobby';
import {Router} from '@angular/router';
import {EventSourcePolyfill} from 'event-source-polyfill';
import UpdateGamePlayerReady from '../models/lobby/update-gameplayer-ready';
import KickPlayer from '../models/lobby/kick-player';
import {environment} from '../../environments/environment';
import {Observable, Observer} from 'rxjs';
import {ModalComponent} from '../components/shared/modal/modal.component';
import {BsModalService} from 'ngx-bootstrap';
import InvitePlayer from '../models/lobby/invite-player';
import UpdateAfkTimer from '../models/lobby/update-afk-timer';
import UpdateTurnTimer from '../models/lobby/update-turn-timer';
import UpdatePreparationTimer from '../models/lobby/update-preparation-timer';
import Team from '../models/game/team';
import {InviteUuid} from '../models/lobby/invite-uuid';
import GameState from '../models/game/gamestate';

@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  private lobby: Lobby;

  constructor(private http: HttpClient,
              private router: Router,
              private modalService: BsModalService) {
  }

  createLobby() {
    this.http.post(environment.apiUrl + environment.lobbyEndPoint, '')
      .subscribe((lobby: Lobby) => {
        this.lobby = lobby;
        this.router.navigate(['lobby']);
      });
  }

  joinPublic() {
    this.http.get(environment.apiUrl + environment.lobbyEndPoint + '/join')
      .subscribe((lobby: Lobby) => {
        this.lobby = lobby;

        if (lobby.gameState === GameState.LOBBY) {
          this.router.navigate(['lobby']);
        } else {
          this.router.navigate(['game']);
        }
      },err =>this.noPublic() );
  }

  updatePrivacy(publicAccess: boolean) {
    const body: UpdatePrivacy = {
      publicAccess
    };

    this.http.patch(environment.apiUrl + environment.lobbyEndPoint + '/privacy', body)
      .subscribe();
  }

  updateBoard(boardType: string) {
    const body: UpdateBoard = {
      boardType
    };

    this.http.patch(environment.apiUrl + environment.lobbyEndPoint + '/board', body)
      .subscribe();
  }

  updatePrepTimer(preparationTimer: number) {
    const body: UpdatePreparationTimer = {
      preparationTimer
    };

    this.http.put(`${environment.apiUrl}${environment.lobbyEndPoint}/preparationtimer`, body)
      .subscribe();
  }

  updateTurnTimer(turnTimer: number) {
    const body: UpdateTurnTimer = {
      turnTimer
    };

    this.http.put(`${environment.apiUrl}${environment.lobbyEndPoint}/turntimer`, body)
      .subscribe();
  }

  updateAfkTimer(afkTimer: number) {
    const body: UpdateAfkTimer = {
      afkTimer
    };

    this.http.put(`${environment.apiUrl}${environment.lobbyEndPoint}/afktimer`, body)
      .subscribe();
  }

  invitePlayer(playerName: string) {
    const body: InvitePlayer = {
      playerName
    };

    this.http.post(environment.apiUrl + environment.lobbyEndPoint + '/invite', body)
      .subscribe();
  }

  getInviteUUID() {
    return this.http.get<InviteUuid>(environment.apiUrl + environment.lobbyEndPoint + '/invite/uuid');
  }

  joinLobby(inviteId: string) {
    this.http.get(environment.apiUrl + environment.lobbyEndPoint + '/invite/' + inviteId)
      .subscribe((response: Lobby) => {
        this.lobby = response;

        this.router.navigate(['lobby']);
      });
  }

  leaveLobby() {
    this.http.delete(environment.apiUrl + environment.lobbyEndPoint + '/leave')
      .subscribe(() => {
        this.router.navigate(['main']);
      });
  }

  updatePlayerReady(ready: boolean) {
    const body: UpdateGamePlayerReady = {
      ready
    };

    this.http.patch(environment.apiUrl + environment.lobbyEndPoint + '/ready', body)
      .subscribe();
  }

  kickPlayer(playerName: string) {
    const body: KickPlayer = {
      playerName
    };

    this.http.request('delete', environment.apiUrl + environment.lobbyEndPoint, {body})
      .subscribe();
  }

  kickCurrentPlayer() {
    this.router.navigate(['main']);
    const initialState = {
      title: 'You\'ve been kicked',
      content: 'The host kicked you from his/her lobby',
      closeBtnName: 'Continue'
    };
    this.modalService.show(ModalComponent, {initialState});
  }

  increaseDifficulty(team: Team) {
    const body = {team};
    this.http.patch(environment.apiUrl + environment.lobbyEndPoint + '/increasedifficulty', body).subscribe()
  }

  decreaseDifficulty(team: Team) {
    const body = {team};
    this.http.patch(environment.apiUrl + environment.lobbyEndPoint + '/decreasedifficulty', body).subscribe()
  }


  startGame() {
    this.http.put(environment.apiUrl + environment.gameEndPoint, {}).subscribe();
  }

  getLobby(): Observable<Lobby> {
    return Observable.create((observer: Observer<Lobby>) => {
      if (this.lobby) {
        observer.next(this.lobby);
        this.lobby = undefined;
      } else {
        this.http.get(environment.apiUrl + environment.lobbyEndPoint)
          .subscribe((lobby: Lobby) => {
            observer.next(lobby);
          });
      }
    });
  }
  noPublic() {
    this.router.navigate(['main']);
    const initialState = {
      title: 'No public game found!',
      content: 'Perhaps start your own? ',
      closeBtnName: 'close'
    };
    this.modalService.show(ModalComponent, {initialState});
  }
}
