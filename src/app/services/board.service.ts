import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http: HttpClient) {
  }

  getBoard() {
    return this.http.get(`${environment.apiUrl}${environment.boardEndpoint}`);
  }

  getReplayBoard(type: string) {
    return this.http.get(`${environment.apiUrl}${environment.boardEndpoint}/${type}`);
  }
}
