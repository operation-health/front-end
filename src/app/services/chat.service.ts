import {Injectable} from '@angular/core';
import {EventSourcePolyfill} from 'event-source-polyfill';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {GameMessage} from '../models/game-message';
import {HttpClient} from '@angular/common/http';
import {StreamService} from './stream.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) {
  }

  sendMessage(message: GameMessage) {
    this.http.post(`${environment.apiUrl}/message`, message).subscribe();
  }
}
