import {Injectable} from '@angular/core';
import {EventSourcePolyfill} from 'event-source-polyfill';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import MovePawn from '../models/board/move-pawn';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) {
  }

  getGameState(): Observable<Object> {
    return this.http.get(`${environment.apiUrl}${environment.gameEndPoint}/state`);
  }

  getRemainingTime() {
    return this.http.get(`${environment.apiUrl}${environment.gameEndPoint}/timer`);
  }

  organisePawn(movePawn: MovePawn) {
    return this.http.put(`${environment.apiUrl}${environment.gameEndPoint}/organise`, movePawn);
  }

  movePawn(movePawn: MovePawn) {
    return this.http.put(`${environment.apiUrl}${environment.gameEndPoint}/move`, movePawn);
  }
}
