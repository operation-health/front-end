import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Account} from '../models/account';
import {Subject} from 'rxjs';
import {ProfilePlayer} from '../models/profile/profile-player';
import {PlayerStats} from '../models/profile/player-stats';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private playerProfile: ProfilePlayer;
  private profileUpdated = new Subject<ProfilePlayer>();

  constructor(private http: HttpClient) {
  }

  forgotPassword(email: string) {
    return this.http.post(environment.apiUrl + environment.playerEndPoint + '/' + email, null);
  }

  editAccount(email?: string, password?: string, profilePicture?: string) {
    const accountDto: Account = {
      email: email,
      password: password,
      profilePicture: profilePicture
    };

    return this.http.put(environment.apiUrl + environment.playerEndPoint, accountDto);
  }

  getFriends() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/friends/friends');
  }

  getFriendRequests() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/friends/requests');
  }

  removeFriend(usernameFriend: string) {
    return this.http.patch(environment.apiUrl + environment.playerEndPoint + '/friends/delete/' + usernameFriend, null);
  }

  searchFriend(searchValue: string) {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/friends/search', {params: {searchValue: searchValue}});
  }

  addFriend(usernameFriend: string) {
    return this.http.patch(environment.apiUrl + environment.playerEndPoint + '/friends/' + usernameFriend, null);
  }

  uploadAvatar(file: File) {
    const formData: FormData = new FormData();
    formData.append('picture', file);

    return this.http.patch(environment.apiUrl + environment.playerEndPoint + '/picture', formData);
  }

  getAvatar() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/picture', {responseType: 'blob'});
  }

  getAvatarFromOtherPlayer(playerName: string) {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/picture/' + playerName, {responseType: 'blob'});
  }

  getStatsOfPlayer(playerName: string) {
    return this.http.get(environment.apiUrl + environment.playerStatsEndpoint + '/' + playerName);
  }

  getProfile() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint).subscribe((response: ProfilePlayer) => {
      this.playerProfile = response;
      this.profileUpdated.next(response);
    });
  }

  getStats(): PlayerStats {
    return this.playerProfile.playerStats;
  }

  getProfileUpdateListener() {
    return this.profileUpdated.asObservable();
  }

  getEmail() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/email');
  }

  getRecentlyPlayed() {
    return this.http.get(environment.apiUrl + environment.playerEndPoint + '/recent');
  }
}
