import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ReplayService {

  constructor(private http: HttpClient, private router: Router) {
  }

  selectReplay(id: number) {
    this.router.navigate(['/replay/' + id]);
  }

  getReplay(gameId: number) {
    return this.http.get(environment.apiUrl + environment.replayEndPoint + '/' + gameId);
  }

  getReplays() {
    return this.http.get(environment.apiUrl + environment.replayEndPoint);
  }
}
