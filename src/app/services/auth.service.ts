import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {HttpClient} from '@angular/common/http';
import {Login} from '../models/login';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {SocialUser} from 'angular-6-social-login';
import {SocialRegister} from '../models/social-register';
import {SocialLogin} from '../models/social-login';
import {StreamService} from './stream.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isAuthenticated = false;
  private token: string;
  private playerName: string;
  private authUser: Login;

  constructor(private http: HttpClient,
              private router: Router,
              private streamService: StreamService) {
  }

  register(playerName: string, email: string, password: string) {
    const user: User = {
      playerName: playerName,
      email: email,
      password: password
    };

    return this.http.post(environment.apiUrl + environment.authEndPoint + '/register', user);
  }

  login(name: string, password: string) {
    const user: Login = {
      playerNameOrEmail: name,
      password: password
    };

    return this.http.post<{ token: string }>(environment.apiUrl + environment.authEndPoint + '/login', user)
      .pipe(map((response: { token: string, playerName: string }) => {
        this.token = response.token;
        this.playerName = response.playerName;

        if (response.token) {
          this.authUser = user;
          this.isAuthenticated = true;
          this.saveAuthToken(this.token, this.playerName);

          this.streamService.resetEventSource(this.token);
        }
      }));
  }

  logout() {
    this.token = null;
    this.playerName = null;
    this.isAuthenticated = false;
    this.authUser = null;

    this.clearAuthUser();

    this.router.navigate(['login']);
  }

  autoAuthUser() {
    const storage = this.getAuthUser();
    this.token = storage.token;
    this.playerName = storage.playerName;
    this.isAuthenticated = true;

      this.http.get(environment.apiUrl + environment.authEndPoint).subscribe(() => {
        this.streamService.resetEventSource(this.token);
      }, () => {
        this.logout();
      });
  }

  registerWithSocial(socialUser: SocialUser, username: string) {
    const socialRegisterDto: SocialRegister = {
      playerName: username,
      token: socialUser.token,
      provider: socialUser.provider
    };

    return this.http.post<{ token: string }>(environment.apiUrl + environment.authEndPoint + '/socialregister', socialRegisterDto);
  }

  loginWithSocial(socialUser: SocialUser) {
    const socialLoginDto: SocialLogin = {
      token: socialUser.token,
      provider: socialUser.provider
    };

    const user: Login = {
      playerNameOrEmail: name,
      password: null
    };

    return this.http.post<{ token: string }>(environment.apiUrl + environment.authEndPoint + '/sociallogin', socialLoginDto)
      .pipe(map((response: { token: string, playerName: string }) => {
        this.token = response.token;
        this.playerName = response.playerName;

        if (response.token) {
          this.authUser = user;
          this.isAuthenticated = true;
          this.saveAuthToken(this.token, this.playerName);
        }
      }));
  }

  usernameExists(username: string) {
    return this.http.get(environment.apiUrl + environment.authEndPoint + '/' + username);
  }

  confirmEmail(token: string) {
    return this.http.patch(environment.apiUrl + environment.authEndPoint + '/confirmEmail/' + token, null);
  }


  private saveAuthToken(token: string, playerName: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('playerName', playerName);
  }

  private clearAuthUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('playerName');
  }

  private getAuthUser() {
    const token = localStorage.getItem('token');
    const playerName = localStorage.getItem('playerName');

    return {
      token: token,
      playerName: playerName
    };
  }

  isAuth() {
    return this.isAuthenticated;
  }

  getToken() {
    return this.token;
  }

  getPlayerName() {
    return this.playerName;
  }
}
