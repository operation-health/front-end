import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {EventSourcePolyfill} from 'event-source-polyfill';
import {environment} from '../../environments/environment';
import ActionEvent from '../models/event/action-event';

@Injectable({
  providedIn: 'root'
})
export class StreamService {

  private readonly actionEventSubject: Subject<ActionEvent>;
  private readonly connectObservable: Subject<undefined>;

  private eventSource;

  constructor() {
    this.actionEventSubject = new Subject();
    this.connectObservable = new Subject();
  }

  getStream(): Observable<ActionEvent> {
    return this.actionEventSubject.asObservable();
  }

  getConnectObservable(): Observable<undefined> {
    return this.connectObservable;
  }

  resetEventSource(token: string) {
    if (token === null) {
      return;
    }
    if (this.eventSource) {
      this.eventSource.close();
    }

    const eventSource = new EventSourcePolyfill(environment.apiUrl + environment.streamEndPoint, {
      headers: {'Authorization': 'Bearer ' + token},
      heartbeatTimeout: 86400000
    });

    const resetEventSourceTimeout = timeout => {
      setTimeout(() => {
        if (eventSource !== this.eventSource) {
          return;
        }
        this.resetEventSource(token);
      }, timeout);
    };

    eventSource.onmessage = (e) => {
      const event = JSON.parse(e.data);
      console.log(event);
      this.actionEventSubject.next(event);
    };
    eventSource.onerror = (e) => {
      if (e.status === 406) {
        console.log('Disconnected from EventSource with status code 406, reconnecting in 10 seconds');
        resetEventSourceTimeout(10000);
        eventSource.close();
        return;
      }
      console.log('Disconnected from EventSource, reconnecting in 2 seconds', e);
      resetEventSourceTimeout(2000);
      eventSource.close();
    };
    eventSource.onopen = () => {
      console.log('Connected to EventSource');
      this.connectObservable.next(undefined);
    };

    this.eventSource = eventSource;
  }
}

