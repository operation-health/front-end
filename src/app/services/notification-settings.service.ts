import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {NotificationSettings} from '../models/notification-settings';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationSettingsService {
  constructor(private http: HttpClient) {
  }

  getSettings() {
    return this.http.get<NotificationSettings>(environment.apiUrl + environment.notificationSettingsEndPoint);
  }

  changeSettings(notificationSettings: NotificationSettings) {
    return this.http.put<NotificationSettings>(environment.apiUrl + environment.notificationSettingsEndPoint, notificationSettings);
  }

}
