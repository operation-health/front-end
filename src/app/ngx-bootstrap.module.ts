import {NgModule} from '@angular/core';
import {ButtonsModule} from 'ngx-bootstrap';

@NgModule({
  exports: [
    ButtonsModule,
  ]
})
export class NgxBootstrapModule {
}
