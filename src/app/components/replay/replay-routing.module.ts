import {ReplayListComponent} from './replay-list/replay-list.component';
import {AuthGuard} from '../../guards/auth.guard';
import {RouterModule, Routes} from '@angular/router';
import {ReplayComponent} from './replay/replay.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: '', component: ReplayListComponent, canActivate: [AuthGuard], pathMatch: 'full'},
  {path: ':id', component: ReplayComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ReplayRoutingModule {
}
