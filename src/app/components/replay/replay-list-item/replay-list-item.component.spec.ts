import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplayListItemComponent} from './replay-list-item.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('ReplayListItemComponent', () => {
  let component: ReplayListItemComponent;
  let fixture: ComponentFixture<ReplayListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplayListItemComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplayListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
