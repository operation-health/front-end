import {Component, Input} from '@angular/core';
import {ReplayService} from '../../../services/replay.service';

@Component({
  selector: 'app-replay-list-item',
  templateUrl: './replay-list-item.component.html',
  styleUrls: ['./replay-list-item.component.scss']
})
export class ReplayListItemComponent {

  @Input() id: number;
  @Input() boardType: string;
  @Input() endTime: string;
  @Input() victory: boolean;

  constructor(private replayService: ReplayService) {
  }

  selectReplay(id: number) {
    this.replayService.selectReplay(id);
  }
}
