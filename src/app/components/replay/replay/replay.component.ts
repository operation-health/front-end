import {Component, OnInit} from '@angular/core';
import {ReplayService} from '../../../services/replay.service';
import {ReplayGame} from '../../../models/replay/replay-game';
import {Subject} from 'rxjs';
import ActionEvent from '../../../models/event/action-event';
import Game from '../../../models/game/game';
import {ActivatedRoute, Router} from '@angular/router';
import Action from '../../../models/event/action';
import GamePawnAttacked from '../../../models/event/actions/game/game-pawn-attacked';
import GameDoubleElimination from '../../../models/event/actions/game/game-double-elimination';
import GameFinalBlow from '../../../models/event/actions/game/game-final-blow';
import GameState from '../../../models/game/gamestate';
import GameFlagKilled from '../../../models/event/actions/game/game-flag-killed';
import GamePassiveWin from '../../../models/event/actions/game/game-passive-win';
import GameHumansEliminated from '../../../models/event/actions/game/game-humans-eliminated';
import GameAttritionKill from '../../../models/event/actions/game/game-attrition-kill';
import GameAttritionVictory from '../../../models/event/actions/game/game-attrition-victory';
import * as _ from 'lodash';
import GamePawnMoved from '../../../models/event/actions/game/game-pawn-moved';
import Pawn from '../../../models/game/pawn';
import Team from '../../../models/game/team';

@Component({
  selector: 'app-replay',
  templateUrl: './replay.component.html',
  styleUrls: ['./replay.component.scss']
})
export class ReplayComponent implements OnInit {

  currentEvent = 0;
  showCards = true;
  replay: ReplayGame;
  playing = false;
  actionEventSubject: Subject<ActionEvent> = new Subject<ActionEvent>();
  gameSubject: Subject<Game> = new Subject<Game>();
  backgrounds = [
    '../../../../assets/img/boards/beach-map.jpg',
    '../../../../assets/img/boards/grass-map.jpg',
    '../../../../assets/img/boards/rocky-map.jpg'
  ];
  backgroundImage = this.backgrounds[0];

  private gameId: number;
  private replayStates: ReplayGame[] = [];
  private stateInterval = 10;
  private timeOut;

  constructor(private replayService: ReplayService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.backgroundImage = this.backgrounds[Math.trunc(Math.random() * this.backgrounds.length)];
    this.route.params.subscribe(params => this.gameId = params['id']);
    this.replayService.getReplay(this.gameId)
      .subscribe((replay: ReplayGame) => {
        this.replay = replay;
        setTimeout(() => this.initializeCheckPointStates(), 1);
      });
  }

  returnToReplays() {
    this.router.navigate(['/replay']);
  }

  initializeCheckPointStates() {
    for (let i = 0; i < this.replay.events.length; i++) {
      if (i % this.stateInterval === 0) {
        this.replayStates.push(_.cloneDeep(this.replay));
      }
      this.handleEvent(this.replay.events[i]);
    }
    this.resetToCheckPointState(0);
  }

  resetToCheckPointState(eventIndex: number) {
    const stateIndex = Math.floor(eventIndex / this.stateInterval);
    this.replay = _.cloneDeep(this.replayStates[stateIndex]);
    this.currentEvent = stateIndex * this.stateInterval;
    this.gameSubject.next(this.replay);
  }

  handleEventsUntilCurrentEvent(eventIndex: number) {
    this.resetToCheckPointState(eventIndex);
    for (this.currentEvent; this.currentEvent < eventIndex; this.currentEvent++) {
      const event = this.replay.events[this.currentEvent];
      this.handleEvent(event);
      this.actionEventSubject.next(event);
    }
  }

  togglePlay() {
    this.playing = !this.playing;
    this.playEvent();
  }

  playEvent() {
    if (this.playing) {
      this.selectNextEvent(1);
      if (this.isPreviousEventAttack()) {
        this.timeOut = setTimeout(() => this.playEvent(), 5000);
      } else if (this.isPreviousEventEndGame()) {
        this.playing = !this.playing;
        clearTimeout(this.timeOut);
      } else {
        this.timeOut = setTimeout(() => this.playEvent(), 1000);
      }
    } else {
      clearTimeout(this.timeOut);
    }
  }

  isPreviousEventAttack() {
    const action = this.replay.events[this.currentEvent - 1].action;
    return action === Action.GAME_PAWN_ATTACKED || action === Action.GAME_ATTRITION_KILL || action === Action.GAME_FLAG_KILLED;
  }

  isPreviousEventEndGame() {
    const action = this.replay.events[this.currentEvent - 1].action;
    return action === Action.GAME_FINAL_BLOW
      || action === Action.GAME_PASSIVE_WIN
      || action === Action.GAME_HUMANS_ELIMINATED
      || action === Action.GAME_ATTRITION_VICTORY;
  }

  previousEvent() {
    this.selectPreviousEvent(1);
  }

  nextEvent() {
    this.selectNextEvent(1);
  }

  fastBackward() {
    this.selectPreviousEvent(5);
  }

  fastForward() {
    this.selectNextEvent(5);
  }

  private selectPreviousEvent(amount: number) {
    this.showCards = false;
    let eventIndex = this.currentEvent - amount;
    if (eventIndex < 0) {
      eventIndex = 0;
    }
    this.handleEventsUntilCurrentEvent(eventIndex);
    setTimeout(() => this.showCards = true, 1);
  }

  private selectNextEvent(amount: number) {
    if (amount > 1) {
      this.showCards = false;
    }
    for (let i = 0; i < amount && this.currentEvent !== this.replay.events.length; i++) {
      const event = this.replay.events[this.currentEvent];
      this.handleEvent(event);
      this.actionEventSubject.next(event);
      this.currentEvent += 1;
    }
    if (!this.showCards) {
      setTimeout(() => this.showCards = true, 1);
    }
  }

  private handleEvent(event: ActionEvent) {
    switch (event.action) {
      case Action.GAME_PAWN_MOVED: {
        const payload = <GamePawnMoved>event.payload;
        this.setPawnBoardTile(payload.pawnId, payload.tileId);
        break;
      }
      case Action.GAME_PAWN_ATTACKED: {
        const payload = <GamePawnAttacked>event.payload;
        const pawns = this.getAllPawns();

        const movedPawn = this.findPawn(pawns, payload.movedPawnId);
        this.setPawnBoardTile(movedPawn, payload.tileId);
        movedPawn.dead = payload.movedPawnDead;

        this.findPawn(pawns, payload.attackedPawnId).dead = payload.attackedPawnDead;
        break;
      }
      case Action.GAME_DOUBLE_ELIMINATION: {
        const payload = <GameDoubleElimination>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = true;
        this.findGamePlayer(payload.attackedTeam).dead = true;
        break;
      }
      case Action.GAME_FINAL_BLOW: {
        const payload = <GameFinalBlow>event.payload;
        this.setPawnBoardTile(payload.movedPawnId, payload.tileId);
        this.findGamePlayer(payload.attackedTeam).dead = true;
        this.replay.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_FLAG_KILLED: {
        const payload = <GameFlagKilled>event.payload;
        this.setPawnBoardTile(payload.movedPawnId, payload.tileId);
        this.findGamePlayer(payload.attackedTeam).dead = true;
        break;
      }
      case Action.GAME_PASSIVE_WIN: {
        const payload = <GamePassiveWin>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = true;
        this.findGamePlayer(payload.attackedTeam).dead = true;
        this.replay.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_HUMANS_ELIMINATED: {
        const payload = <GameHumansEliminated>event.payload;
        this.setPawnBoardTile(payload.movedPawnId, payload.tileId);
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = payload.attackedTeamDead;
        this.replay.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_ATTRITION_KILL: {
        const payload = <GameAttritionKill>event.payload;
        this.setPawnBoardTile(payload.movedPawnId, payload.tileId);
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = !payload.currentTeamDead;
        break;
      }
      case Action.GAME_ATTRITION_VICTORY: {
        const payload = <GameAttritionVictory>event.payload;
        this.setPawnBoardTile(payload.movedPawnId, payload.tileId);
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = !payload.currentTeamDead;
        this.replay.gameState = GameState.FINISHED;
        break;
      }
    }
  }

  private getAllPawns() {
    return this.replay.gamePlayers.map(gp => gp.pawns).reduce((a, b) => a.concat(b));
  }

  private findPawn(pawns: Pawn[], id: number) {
    return pawns.find(pawn => pawn.id === id);
  }

  private findGamePlayer(team: Team) {
    return this.replay.gamePlayers.find(gp => gp.team === team);
  }

  private setPawnBoardTile(pawn: Pawn | number, tileId: number) {
    if (typeof pawn === 'number') {
      pawn = this.findPawn(this.getAllPawns(), pawn);
    }
    pawn.boardTile = {
      tileId,
      connectedTileIds: []
    };
  }
}
