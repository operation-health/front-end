import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplayComponent} from './replay.component';
import {ReplayService} from '../../../services/replay.service';
import {GameHeaderComponent} from '../../shared/game/game-header/game-header.component';
import {AttackCardsComponent} from '../../shared/game/attackcards/attack-cards.component';
import {BoardComponent} from '../../shared/game/board/board.component';
import {ProgressbarModule} from 'ngx-bootstrap';
import {FormatTimePipe} from '../../../pipes/format-time.pipe';
import {CardComponent} from '../../shared/game/card/card.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('ReplayComponent', () => {
  let component: ReplayComponent;
  let fixture: ComponentFixture<ReplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        ReplayService
      ],
      imports: [
        ProgressbarModule.forRoot(),
        InlineSVGModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        ReplayComponent,
        GameHeaderComponent,
        AttackCardsComponent,
        BoardComponent,
        FormatTimePipe,
        CardComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
