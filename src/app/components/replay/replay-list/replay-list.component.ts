import {Component, OnInit} from '@angular/core';
import {ReplayListItem} from '../../../models/replay/replay-list-item';
import {ReplayService} from '../../../services/replay.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-replay-list',
  templateUrl: './replay-list.component.html',
  styleUrls: ['./replay-list.component.scss']
})
export class ReplayListComponent implements OnInit {

  replays: ReplayListItem[] = [];

  constructor(private replayService: ReplayService,
              private router: Router) {
  }

  ngOnInit() {
    this.replayService.getReplays()
      .subscribe((response: ReplayListItem[]) => {
        this.replays = response;
      });
  }

  returnToMenu() {
    this.router.navigate(['/main']);
  }
}
