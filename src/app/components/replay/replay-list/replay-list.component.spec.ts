import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReplayListComponent} from './replay-list.component';
import {ReplayListItemComponent} from "../replay-list-item/replay-list-item.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";

describe('ReplayListComponent', () => {
  let component: ReplayListComponent;
  let fixture: ComponentFixture<ReplayListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        ReplayListComponent,
        ReplayListItemComponent
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplayListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
