import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReplayListItemComponent} from './replay-list-item/replay-list-item.component';
import {ReplayComponent} from './replay/replay.component';
import {ReplayListComponent} from './replay-list/replay-list.component';
import {ReplayRoutingModule} from './replay-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ProgressbarModule} from 'ngx-bootstrap';

@NgModule({
  declarations: [
    ReplayComponent,
    ReplayListComponent,
    ReplayListItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReplayRoutingModule,
    ProgressbarModule.forRoot()
  ]
})
export class ReplayModule {
}
