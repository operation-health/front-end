import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  toShow: String = 'dates';

  constructor(private router: Router) {
  }

  ngOnInit() {

  }

  returnToMenu() {
    this.router.navigate(['/main.menu']);
  }
  redirectGraph(directURL:String){
    window.open( "https://public.tableau.com/profile/annelies.vermeiren#!/vizhome/ISMTableau/"+directURL);
  }
}
