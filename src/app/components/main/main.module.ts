import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {RulesComponent} from './rules/rules.component';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../environments/environment';
import {StatisticsComponent} from './statistics/statistics.component';
import {TabsModule} from "ngx-bootstrap";

@NgModule({
  declarations: [
    MainMenuComponent,
    RulesComponent,
    StatisticsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    NgxBootstrapModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    TabsModule.forRoot()
  ]
})
export class MainModule {
}
