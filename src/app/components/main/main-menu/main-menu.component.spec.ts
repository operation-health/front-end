import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MainMenuComponent} from './main-menu.component';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {ModalModule} from 'ngx-bootstrap';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BottomBarComponent} from '../../shared/bottom-bar/bottom-bar.component';
import {FriendsComponent} from '../../shared/friends/friends.component';
import {ChatComponent} from '../../shared/chat/chat.component';
import {AuthService} from '../../../services/auth.service';
import {LobbyService} from '../../../services/lobby.service';
import {AngularFireMessaging, AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {DynamicModule} from 'ng-dynamic-component';
import {environment} from '../../../../environments/environment';

describe('MainMenuComponent', () => {
  let component: MainMenuComponent;
  let fixture: ComponentFixture<MainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        AngularFireMessagingModule,
        AngularFireModule.initializeApp(environment.firebase),
        HttpClientTestingModule,
        RouterTestingModule,
        ModalModule.forRoot(),
        DynamicModule
      ],
      declarations: [
        MainMenuComponent,
        BottomBarComponent,
        FriendsComponent,
        ChatComponent
      ],
      providers: [
        AuthService,
        LobbyService
      ]
    }).compileComponents();
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ChatComponent, FriendsComponent]
      }
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterEach(() => {
    fixture.destroy();
  });
});
