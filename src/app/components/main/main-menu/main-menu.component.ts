import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {LobbyService} from '../../../services/lobby.service';
import {MessagingService} from '../../../services/messaging.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  title = 'Welcome!';
  private playerName: string;

  constructor(private authService: AuthService, private lobbyService: LobbyService, private messagingService: MessagingService) {

  }

  ngOnInit() {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();

    this.playerName = this.authService.getPlayerName();
    if (this.playerName) {
      this.title = 'Welcome ' + this.playerName + '!';
    }
  }

  createLobby() {
    this.lobbyService.createLobby();
  }

  joinPublic() {
    this.lobbyService.joinPublic();
  }

  onLogOut() {
    this.authService.logout();
  }
}
