import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent {
  activePawn: string;

  pawns = [
    {
      name: 'Flag',
      description: 'The flag is an unmovable pawn and can be captured by any movable pawn.'
    },
    {
      name: 'Bomb',
      description: 'It is unmovable once the preparation phase is over. A bomb will kill every pawn attacking it, unless it\'s a miner.'
    },
    {
      name: 'Spy',
      description: 'The spy has the lowest rank of all pawns. It can be defeated by every higher ranked pawn. However, when the spy is being used to' +
        'attack the marshal, it will defeat it. If the marshal attacks the spy, the spy dies.'
    },
    {
      name: 'Scout',
      description: 'A scout can kill the spy and take the flag. However, it can be killed by every other higher ranked pawn.'
    },
    {
      name: 'Miner',
      description: 'A miner can kill the spy, scout, bomb and take the flag, However, it can be killed by every other higher ranked pawn.'
    },
    {
      name: 'Sergeant',
      description: 'A sergeant can kill the spy, scout, miner and take the flag. However, it will be defeated by every other higher ranked pawn.'
    },
    {
      name: 'Lieutenant',
      description: 'A lieutenant can kill the spy, scout, miner, sergeant and take the flag. However, it will be killed by every other higher ranked pawn.'
    },
    {
      name: 'Captain',
      description: 'A captain can kill the spy, scout, miner, sergeant, lieutenant and take the flag. However, it will be killed by every other higher ranked pawn.'
    },
    {
      name: 'Major',
      description: 'A major can kill the spy, scout, miner, sergeant, lieutenant, captain and take the flag. However, it will be killed by every other higher ranked pawn.'
    },
    {
      name: 'Colonel',
      description: 'A colonel can kill the spy, scout, miner, sergeant, lieutenant, captain, major and take the flag. However, it can be killed by every other higher ranked pawn.'
    },
    {
      name: 'General',
      description: 'A general can kill the spy, scout, miner, sergeant, lieutenant, captain, colonel and take the flag. However, it can be killed by every other higher ranked pawn.'
    },
    {
      name: 'Marshal',
      description: 'A marshal can kill the spy, scout, miner, sergeant, lieutenant, captain, colonel, general and take the flag. A marshal will be killed when attacked by a spy.'
    }
  ];

  constructor(private router: Router) {
  }

  returnToMenu() {
    this.router.navigate(['/main.menu']);
  }

  pawnClick(name: string) {
    this.activePawn = name;
  }

  getDescription() {
    return this.pawns.find(p => p.name === this.activePawn).description;
  }
}
