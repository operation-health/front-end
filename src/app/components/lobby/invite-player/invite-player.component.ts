import {Component} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {NgForm} from '@angular/forms';
import {LobbyService} from '../../../services/lobby.service';
import {FacebookService, InitParams, UIParams, UIResponse} from 'ngx-facebook';
import {environment} from '../../../../environments/environment.prod';


@Component({
  selector: 'app-invite-player',
  templateUrl: './invite-player.component.html',
  styleUrls: ['./invite-player.component.scss'],
  providers: [FacebookService]
})
export class InvitePlayerComponent {


  constructor(public bsModalRef: BsModalRef, private lobbyService: LobbyService, private facebookService: FacebookService) {
    const fbParams: InitParams = {
      appId: '766159310406307',
      xfbml: false,
      version: 'v2.8'
    };
    this.facebookService.init(fbParams).then(() => console.log('init called')).catch(data => console.log(data));
  }

  onInvite(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.lobbyService.invitePlayer(form.value.username);
    this.bsModalRef.hide();
  }

  inviteWithFacebook() {
    this.lobbyService.getInviteUUID().subscribe(data => {
      const options: UIParams = {
        method: 'send',
        link: location.origin + '/invite/' + data.inviteUUID,

      };
      this.facebookService.ui(options)
        .then(() => {
          console.log('Dialog opened');
        });
    });
  }
}
