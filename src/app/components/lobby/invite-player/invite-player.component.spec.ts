import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InvitePlayerComponent} from './invite-player.component';
import {FormsModule} from '@angular/forms';
import {BsModalRef, BsModalService, ModalModule} from 'ngx-bootstrap';
import {LobbyService} from '../../../services/lobby.service';
import {RouterTestingModule} from '@angular/router/testing';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('InvitePlayerComponent', () => {
  let component: InvitePlayerComponent;
  let fixture: ComponentFixture<InvitePlayerComponent>;
  let lobbyService: LobbyService;
  let debugElement: DebugElement;
  let lobbySpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvitePlayerComponent],
      providers: [
        BsModalRef,
        LobbyService,
        BsModalService
      ],
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ModalModule.forRoot()
      ]
    })
      .compileComponents();
    fixture = TestBed.createComponent(InvitePlayerComponent);
    debugElement = fixture.debugElement;
    lobbyService = debugElement.injector.get(LobbyService);
    lobbySpy = spyOn(lobbyService, 'invitePlayer').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onInvite', () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
    spyOn(component, 'onInvite');
    fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);
    expect(component.onInvite).toHaveBeenCalled();
  });
});
