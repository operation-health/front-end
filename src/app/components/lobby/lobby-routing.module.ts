import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../guards/auth.guard';
import {LobbyComponent} from './lobby/lobby.component';
import {JoinLobbyComponent} from './join-lobby/join-lobby.component';

const routes: Routes = [
  {path: '', component: LobbyComponent, canActivate: [AuthGuard], pathMatch: 'full'},
  {path: 'invite/:inviteId', component: JoinLobbyComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class LobbyRoutingModule {
}
