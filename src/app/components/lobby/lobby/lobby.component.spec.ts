import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LobbyComponent} from './lobby.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {LobbyService} from '../../../services/lobby.service';
import {AuthService} from '../../../services/auth.service';
import {BsModalService, ModalModule} from 'ngx-bootstrap';
import {NgxBootstrapModule} from '../../../ngx-bootstrap.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StreamService} from '../../../services/stream.service';

describe('LobbyComponent', () => {
  let component: LobbyComponent;
  let fixture: ComponentFixture<LobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LobbyComponent],
      providers: [
        FormBuilder,
        LobbyService,
        AuthService,
        BsModalService,
        StreamService
      ],
      imports: [
        ReactiveFormsModule,
        SwiperModule,
        NgxBootstrapModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ModalModule.forRoot(),
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
