import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SwiperConfigInterface} from 'ngx-swiper-wrapper';
import {LobbyService} from '../../../services/lobby.service';
import {Lobby} from '../../../models/lobby/lobby';
import {Subscription} from 'rxjs';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {InvitePlayerComponent} from '../invite-player/invite-player.component';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {StreamService} from '../../../services/stream.service';
import Action from '../../../models/event/action';
import {LobbyPlayerJoined} from '../../../models/event/actions/lobby/lobby-player-joined';
import {LobbyPlayerLeft} from '../../../models/event/actions/lobby/lobby-player-left';
import {LobbyHostLeft} from '../../../models/event/actions/lobby/lobby-host-left';
import {LobbyPlayerKicked} from '../../../models/event/actions/lobby/lobby-player-kicked';
import {LobbyPlayerReadyChanged} from '../../../models/event/actions/lobby/lobby-player-ready-changed';
import {LobbyBoardChanged} from '../../../models/event/actions/lobby/lobby-board-changed';
import {LobbyAccessChanged} from '../../../models/event/actions/lobby/lobby-access-changed';
import {LobbyPrepTimerChanged} from '../../../models/event/actions/lobby/lobby-prep-timer-changed';
import {LobbyTurnTimerChanged} from '../../../models/event/actions/lobby/lobby-turn-timer-changed';
import {LobbyAfkTimerChanged} from '../../../models/event/actions/lobby/lobby-afk-timer-changed';
import GamePlayer from '../../../models/game/gameplayer';
import {LobbyAiJoined} from '../../../models/event/actions/lobby/lobby-ai-joined';
import {LobbyAiLeft} from '../../../models/event/actions/lobby/lobby-ai-left';
import {LobbyAiUpdated} from '../../../models/event/actions/lobby/lobby-ai-updated';
import Team from '../../../models/game/team';
import {LobbyStatsComponent} from '../../shared/lobby-stats/lobby-stats.component';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {

  lobby: Lobby;
  leaveProgress: number;

  sortedGamePlayers: GamePlayer[];
  private lobbySub: Subscription;
  private streamSub: Subscription;
  private streamConnectSub: Subscription;

  boards = [
    {type: 'WATERLOO', url: 'assets/img/boards/waterloo.jpg'},
    {type: 'PASSENDALE', url: 'assets/img/boards/passendale.jpg'}
  ];

  bsModalRef: BsModalRef;
  public config: SwiperConfigInterface = {
    navigation: false,
    allowTouchMove: false
  };

  constructor(private formBuilder: FormBuilder,
              private lobbyService: LobbyService,
              private authService: AuthService,
              private streamService: StreamService,
              private modalService: BsModalService,
              private router: Router) {
  }

  ngOnInit() {
    this.syncLobby();
  }

  ngOnDestroy(): void {
    if (this.lobbySub) {
      this.lobbySub.unsubscribe();
    }
    if (this.streamSub) {
      this.streamSub.unsubscribe();
    }
    if (this.streamConnectSub) {
      this.streamConnectSub.unsubscribe();
    }
  }

  syncLobby() {
    if (this.lobbySub) {
      this.lobbySub.unsubscribe();
    }
    this.lobbySub = this.lobbyService.getLobby().subscribe((lobby: Lobby) => {
      this.lobby = lobby;
      this.updateSwiperConfig();
      this.updateSortedGamePlayers();
      this.listenToStream();
    });
  }

  listenToStream() {
    if (!this.streamConnectSub) {
      this.streamConnectSub = this.streamService.getConnectObservable().subscribe(() => this.syncLobby());
    }
    if (!this.streamSub) {
      this.streamSub = this.streamService.getStream()
        .subscribe(event => {
          switch (event.action) {
            case Action.LOBBY_PLAYER_JOINED: {
              const payload = <LobbyPlayerJoined>event.payload;
              if (this.lobby.gamePlayers
                .filter(gp => !gp.computer)
                .map(gp => gp.player.playerName)
                .includes(payload.gamePlayer.player.playerName)) {
                return;
              }
              this.lobby.gamePlayers.push(payload.gamePlayer);
              this.updateSortedGamePlayers();
              break;
            }
            case Action.LOBBY_AI_JOINED: {
              const payload = <LobbyAiJoined>event.payload;
              this.lobby.gamePlayers.push(payload.gamePlayer);
              this.updateSortedGamePlayers();
              break;
            }
            case Action.LOBBY_AI_LEFT: {
              const payload = <LobbyAiLeft>event.payload;
              this.lobby.gamePlayers = this.lobby.gamePlayers.filter(gp => gp.team !== payload.team);
              this.updateSortedGamePlayers();
              break;
            }
            case Action.LOBBY_PLAYER_LEFT: {
              const payload = <LobbyPlayerLeft>event.payload;
              this.lobby.gamePlayers = this.lobby.gamePlayers.filter(gp => gp.computer || gp.player.playerName !== payload.playerName);
              this.updateSortedGamePlayers();
              break;
            }
            case Action.LOBBY_HOST_LEFT: {
              const payload = <LobbyHostLeft>event.payload;
              this.lobby.gamePlayers = this.lobby.gamePlayers.filter(gp => gp.computer || gp.player.playerName !== payload.hostPlayerName);
              this.lobby.gamePlayers.find(gp => !gp.computer && gp.player.playerName === payload.newHostPlayerName).host = true;
              this.updateSortedGamePlayers();
              this.updateSwiperConfig();
              break;
            }
            case Action.LOBBY_PLAYER_KICKED: {
              const payload = <LobbyPlayerKicked>event.payload;
              if (this.getCurrentGamePlayer().player.playerName === payload.playerName) {
                this.lobbyService.kickCurrentPlayer();
              } else {
                this.lobby.gamePlayers = this.lobby.gamePlayers.filter(gp => gp.computer || gp.player.playerName !== payload.playerName);
              }
              this.updateSortedGamePlayers();
              break;
            }
            case Action.LOBBY_PLAYER_READY_CHANGED: {
              const payload = <LobbyPlayerReadyChanged>event.payload;
              this.lobby.gamePlayers.find(gp => !gp.computer && gp.player.playerName === payload.playerName).ready = payload.ready;
              break;
            }
            case Action.LOBBY_BOARD_CHANGED: {
              const payload = <LobbyBoardChanged>event.payload;
              this.lobby.board = payload.board;
              break;
            }
            case Action.LOBBY_ACCESS_CHANGED: {
              const payload = <LobbyAccessChanged>event.payload;
              this.lobby.publicAccess = payload.access;
              break;
            }
            case Action.LOBBY_PREP_TIMER_CHANGED: {
              const payload = <LobbyPrepTimerChanged>event.payload;
              this.lobby.gameRules.preparationTimer = payload.preparationTimer;
              break;
            }
            case Action.LOBBY_TURN_TIMER_CHANGED: {
              const payload = <LobbyTurnTimerChanged>event.payload;
              this.lobby.gameRules.turnTimer = payload.turnTimer;
              break;
            }
            case Action.LOBBY_AFK_TIMER_CHANGED: {
              const payload = <LobbyAfkTimerChanged>event.payload;
              this.lobby.gameRules.afkTimer = payload.afkTimer;
              break;
            }
            case Action.LOBBY_AI_UPDATED: {
              const payload = <LobbyAiUpdated>event.payload;
              this.lobby.gamePlayers.find(gp => gp.team === payload.gamePlayer.team).difficulty = payload.gamePlayer.difficulty;
              break;
            }
            case Action.LOBBY_GAME_STARTED: {
              this.router.navigate(['game']);
              break;
            }
          }
        });
    }
  }

  getCurrentGamePlayer() {
    if (this.lobby) {
      return this.lobby.gamePlayers.filter(gp => !gp.computer).find(gp => gp.player.playerName === this.authService.getPlayerName());
    }
  }

  getTitle() {
    if (this.getCurrentGamePlayer()) {
      const currentGamePlayer = this.getCurrentGamePlayer();
      if (currentGamePlayer.host) {
        return 'Create a game';
      }
      const hostGamePlayer = this.lobby.gamePlayers.find(gp => gp.host);
      return `${hostGamePlayer.player.playerName}'s lobby`;
    }
  }

  getBoardIndex() {
    if (this.lobby) {
      return this.boards.findIndex(board => board.type === this.lobby.board.type);
    }
  }

  isHost() {
    if (this.getCurrentGamePlayer()) {
      return this.getCurrentGamePlayer().host;
    }
  }

  areGamePlayerReady() {
    return this.lobby.gamePlayers
      .filter(gp => !gp.host && !gp.computer)
      .every(gp => gp.ready);
  }

  handleGameStart() {
    if (this.isHost()) {
      this.lobbyService.startGame();
    }
  }

  handleLeave(e) {
    this.leaveProgress = e / 10;
    if (this.leaveProgress === 100) {
      this.lobbyService.leaveLobby();
    }
  }

  handleBoardChange(index) {
    if (this.isHost()) {
      this.lobbyService.updateBoard(this.boards[index].type);
    }
  }

  handleVisibilityChange(isPublic) {
    if (this.isHost()) {
      this.lobbyService.updatePrivacy(isPublic);
    }
  }

  handlePrepTimerChange(event) {
    if (this.isHost()) {
      this.lobbyService.updatePrepTimer(event.target.value);
    }
  }

  handleTurnTimerChange(event) {
    if (this.isHost()) {
      this.lobbyService.updateTurnTimer(event.target.value);
    }
  }

  handleStatsShow(playerName: string) {
    const config = {
      initialState: {
        playerName: playerName
      },
      animated: true
    };
    this.bsModalRef = this.modalService.show(LobbyStatsComponent, config);
  }

  handleAfkTimerChange(event) {
    if (this.isHost()) {
      this.lobbyService.updateAfkTimer(event.target.value);
    }
  }

  handleReadyChange() {
    this.lobbyService.updatePlayerReady(!this.getCurrentGamePlayer().ready);
  }

  handlePlayerKick(playerName: string) {
    if (this.isHost()) {
      this.lobbyService.kickPlayer(playerName);
    }
  }

  handleIncreaseDifficulty(team: Team) {
    this.lobbyService.increaseDifficulty(team);
  }

  handleDecreaseDifficulty(team: Team) {
    this.lobbyService.decreaseDifficulty(team);
  }

  updateSwiperConfig() {
    const gamePlayer = this.getCurrentGamePlayer();
    this.config.navigation = gamePlayer.host;
    this.config.allowTouchMove = gamePlayer.host;
  }

  updateSortedGamePlayers() {
    this.sortedGamePlayers = [];
    this.sortedGamePlayers.push(this.lobby.gamePlayers.find(gp => gp.host));
    this.sortedGamePlayers = this.sortedGamePlayers.concat(this.lobby.gamePlayers.filter(gp => !gp.host && !gp.computer));
    this.sortedGamePlayers = this.sortedGamePlayers.concat(this.lobby.gamePlayers.filter(gp => gp.computer));
  }

  openInviteModal() {
    const config = {animated: true};
    this.bsModalRef = this.modalService.show(InvitePlayerComponent, config);
  }
}
