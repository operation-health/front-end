import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {JoinLobbyComponent} from './join-lobby.component';
import {RouterTestingModule} from '@angular/router/testing';
import {LobbyService} from '../../../services/lobby.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BsModalService, ModalModule} from 'ngx-bootstrap';

describe('JoinLobbyComponent', () => {
  let component: JoinLobbyComponent;
  let fixture: ComponentFixture<JoinLobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JoinLobbyComponent],
      providers: [
        LobbyService,
        BsModalService
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ModalModule.forRoot()
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinLobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
