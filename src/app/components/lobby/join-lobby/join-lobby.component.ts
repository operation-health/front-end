import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {LobbyService} from '../../../services/lobby.service';

@Component({
  selector: 'app-join-lobby',
  templateUrl: './join-lobby.component.html',
  styleUrls: ['./join-lobby.component.scss']
})
export class JoinLobbyComponent implements OnInit {

  constructor(public route: ActivatedRoute, private lobbyService: LobbyService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('inviteId')) {
        this.lobbyService.joinLobby(paramMap.get('inviteId'));
      }
    });
  }

}
