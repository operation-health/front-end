import {NgModule} from '@angular/core';
import {LobbyComponent} from './lobby/lobby.component';
import {JoinLobbyComponent} from './join-lobby/join-lobby.component';
import {InvitePlayerComponent} from './invite-player/invite-player.component';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalModule, TooltipModule} from 'ngx-bootstrap';
import {LobbyRoutingModule} from './lobby-routing.module';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';
import {SafeUrlPipe} from '../../pipes/safe-url.pipe';
import {HoldableDirective} from '../../directives/holdable.directive';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 10,
  slidesPerView: 3,
  centeredSlides: true
};

@NgModule({
  declarations: [
    LobbyComponent,
    JoinLobbyComponent,
    InvitePlayerComponent,
    HoldableDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgxBootstrapModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    SwiperModule,
    LobbyRoutingModule,
    TooltipModule.forRoot()
  ],
  providers: [
    {provide: SWIPER_CONFIG, useValue: DEFAULT_SWIPER_CONFIG, multi: false},
    SafeUrlPipe
  ],
  entryComponents: [
    InvitePlayerComponent
  ]
})
export class LobbyModule {
}
