import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameComponent} from './game.component';
import {GameService} from '../../../services/game.service';
import {GameHeaderComponent} from '../../shared/game/game-header/game-header.component';
import {BoardComponent} from '../../shared/game/board/board.component';
import {FinishScreenComponent} from '../finish-screen/finish-screen.component';
import {FormatTimePipe} from '../../../pipes/format-time.pipe';
import {InlineSVGModule} from 'ng-inline-svg';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AttackCardsComponent} from '../../shared/game/attackcards/attack-cards.component';
import {CardComponent} from '../../shared/game/card/card.component';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        GameService
      ],
      imports: [
        InlineSVGModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        GameComponent,
        GameHeaderComponent,
        BoardComponent,
        FinishScreenComponent,
        FormatTimePipe,
        AttackCardsComponent,
        CardComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
