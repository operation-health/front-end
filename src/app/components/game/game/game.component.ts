import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameService} from '../../../services/game.service';
import Game from '../../../models/game/game';
import GameState from '../../../models/game/gamestate';
import Action from '../../../models/event/action';
import {Subject, Subscription} from 'rxjs';
import {StreamService} from '../../../services/stream.service';
import ActionEvent from '../../../models/event/action-event';
import GamePlayerAfk from '../../../models/event/actions/game/game-player-afk';
import Team from '../../../models/game/team';
import GamePawnAttacked from '../../../models/event/actions/game/game-pawn-attacked';
import GameFinalBlow from '../../../models/event/actions/game/game-final-blow';
import GameFlagKilled from '../../../models/event/actions/game/game-flag-killed';
import GamePawnMoved from '../../../models/event/actions/game/game-pawn-moved';
import GamePlayerReadyChanged from '../../../models/event/actions/game/game-player-ready-changed';
import GameTransitionToPlaying from '../../../models/event/actions/game/game-transition-to-playing';
import MovePawn from '../../../models/board/move-pawn';
import GameDoubleElimination from '../../../models/event/actions/game/game-double-elimination';
import GamePassiveWin from '../../../models/event/actions/game/game-passive-win';
import GameHumansEliminated from '../../../models/event/actions/game/game-humans-eliminated';
import GameAttritionKill from '../../../models/event/actions/game/game-attrition-kill';
import GameAttritionVictory from '../../../models/event/actions/game/game-attrition-victory';
import Pawn from '../../../models/game/pawn';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  game: Game;
  actionEventSubject: Subject<ActionEvent> = new Subject<ActionEvent>();
  gameSubject: Subject<Game> = new Subject<Game>();
  backgrounds = [
    '../../../../assets/img/boards/beach-map.jpg',
    '../../../../assets/img/boards/grass-map.jpg',
    '../../../../assets/img/boards/rocky-map.jpg'
  ];
  backgroundImage = this.backgrounds[0];

  private gameStreamSub: Subscription;
  private connectStreamSub: Subscription;

  constructor(private gameService: GameService, private streamService: StreamService) {
  }

  ngOnInit() {
    this.backgroundImage = this.backgrounds[Math.trunc(Math.random() * this.backgrounds.length)];

    this.syncGameState();
  }

  ngOnDestroy() {
    if (this.gameStreamSub) {
      this.gameStreamSub.unsubscribe();
    }
    if (this.connectStreamSub) {
      this.connectStreamSub.unsubscribe();
    }
  }

  syncGameState() {
    this.gameService.getGameState().subscribe((game: Game) => {
      this.game = game;
      setTimeout(() => this.gameSubject.next(game), 1);
      if (!this.gameStreamSub) {
        this.gameStreamSub = this.streamService.getStream().subscribe(event => this.handleEvent(event));
      }
      if (!this.connectStreamSub) {
        this.connectStreamSub = this.streamService.getConnectObservable().subscribe(() => {
          this.syncGameState();
        });
      }
    });
  }

  handleEvent(event: ActionEvent) {
    switch (event.action) {
      case Action.GAME_PLAYER_AFK: {
        const payload = <GamePlayerAfk>event.payload;
        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, true);
        break;
      }
      case Action.GAME_PAWN_ATTACKED: {
        const payload = <GamePawnAttacked>event.payload;
        const pawns = this.getAllPawns();

        this.findPawn(pawns, payload.movedPawnId).dead = payload.movedPawnDead;
        this.findPawn(pawns, payload.attackedPawnId).dead = payload.attackedPawnDead;

        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, false);
        break;
      }
      case Action.GAME_DOUBLE_ELIMINATION: {
        const payload = <GameDoubleElimination>event.payload;

        this.findGamePlayer(payload.currentTeam).dead = true;
        this.findGamePlayer(payload.attackedTeam).dead = true;

        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, false);
        break;
      }
      case Action.GAME_FINAL_BLOW: {
        const payload = <GameFinalBlow>event.payload;
        this.findGamePlayer(payload.attackedTeam).dead = true;
        this.game.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_FLAG_KILLED: {
        const payload = <GameFlagKilled>event.payload;
        this.findGamePlayer(payload.attackedTeam).dead = true;

        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, false);
        break;
      }
      case Action.GAME_PASSIVE_WIN: {
        const payload = <GamePassiveWin>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = true;
        this.findGamePlayer(payload.attackedTeam).dead = true;
        this.game.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_HUMANS_ELIMINATED: {
        const payload = <GameHumansEliminated>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = payload.attackedTeamDead;
        this.game.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_ATTRITION_KILL: {
        const payload = <GameAttritionKill>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = !payload.currentTeamDead;

        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, false);
        break;
      }
      case Action.GAME_ATTRITION_VICTORY: {
        const payload = <GameAttritionVictory>event.payload;
        this.findGamePlayer(payload.currentTeam).dead = payload.currentTeamDead;
        this.findGamePlayer(payload.attackedTeam).dead = !payload.currentTeamDead;
        this.game.gameState = GameState.FINISHED;
        break;
      }
      case Action.GAME_PAWN_MOVED: {
        const payload = <GamePawnMoved>event.payload;

        this.updateNextTurn(payload.nextTeam);
        this.updateAfk(payload.currentTeam, false);
        break;
      }
      case Action.GAME_PLAYER_READY_CHANGED: {
        const payload = <GamePlayerReadyChanged>event.payload;
        this.findGamePlayer(payload.team).ready = payload.ready;
        break;
      }
      case Action.GAME_TRANSITION_TO_PLAYING: {
        const payload = <GameTransitionToPlaying>event.payload;
        this.game = payload.game;
        break;
      }
    }
    this.actionEventSubject.next(event);
  }

  movePawn(movePawn: MovePawn) {
    const observable = this.game.gameState === GameState.PREPARING ?
      this.gameService.organisePawn(movePawn) : this.gameService.movePawn(movePawn);
    observable.subscribe(undefined, () => this.syncGameState());
  }

  private updateNextTurn(turnTeam: Team) {
    this.findGamePlayer(turnTeam).currentTurn = true;
    this.game.gamePlayers.filter(gp => gp.team !== turnTeam).forEach(gp => gp.currentTurn = false);
  }

  private updateAfk(team: Team, afk: boolean) {
    this.findGamePlayer(team).afk = afk;
  }

  private getAllPawns() {
    return this.game.gamePlayers.map(gp => gp.pawns).reduce((a, b) => a.concat(b));
  }

  private findPawn(pawns: Pawn[], id: number) {
    return pawns.find(pawn => pawn.id === id);
  }

  private findGamePlayer(team: Team) {
    return this.game.gamePlayers.find(gp => gp.team === team);
  }
}
