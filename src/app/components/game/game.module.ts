import {NgModule} from '@angular/core';
import {GameComponent} from './game/game.component';
import {CommonModule} from '@angular/common';
import {InlineSVGModule} from 'ng-inline-svg';
import {GameRoutingModule} from './game-routing.module';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';
import {SharedModule} from '../shared/shared.module';
import {FinishScreenComponent} from "./finish-screen/finish-screen.component";

@NgModule({
  declarations: [
    GameComponent,
    FinishScreenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxBootstrapModule,
    InlineSVGModule.forRoot(),
    GameRoutingModule
  ],
  providers: []
})
export class GameModule {
}
