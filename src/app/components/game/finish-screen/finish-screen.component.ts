import {Component, Input, OnInit} from '@angular/core';
import GamePlayer from '../../../models/game/gameplayer';
import GameState from '../../../models/game/gamestate';
import {Router} from '@angular/router';

@Component({
  selector: 'app-finish-screen',
  templateUrl: './finish-screen.component.html',
  styleUrls: ['./finish-screen.component.scss']
})
export class FinishScreenComponent implements OnInit {

  @Input() gameState: GameState;
  @Input() gamePlayers: GamePlayer[];

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  isGameFinished() {
    return this.gameState === GameState.FINISHED;
  }

  existsWinningGamePlayer() {
    return this.gamePlayers.filter(gp => !gp.computer).some(gp => !gp.dead);
  }

  findWinningGamePlayer() {
    return this.gamePlayers.filter(gp => !gp.computer).find(gp => !gp.dead);
  }

  returnToMenu() {
    this.router.navigate(['main']);
  }
}
