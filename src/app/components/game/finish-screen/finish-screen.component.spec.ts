import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishScreenComponent } from './finish-screen.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('FinishScreenComponent', () => {
  let component: FinishScreenComponent;
  let fixture: ComponentFixture<FinishScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ FinishScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
