import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {LoginComponent} from '../auth/login/login.component';
import {FormsModule} from '@angular/forms';
import {BsModalRef, BsModalService, ModalModule} from 'ngx-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthService, AuthServiceConfig} from 'angular-6-social-login';
import {getAuthServiceConfigs} from '../auth/choose-username/choose-username.component.spec';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        LoginComponent],
      providers: [
        BsModalService,
        BsModalRef,
        AuthService,
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs}
      ],
      imports: [FormsModule,
        ModalModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
