import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {
  token: string;
  private sub: any;
  confirmation = false;
  message = 'Email is confirmed!';

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.token = params['token'];
    });
    this.confirmAccount(this.token);
  }

  confirmAccount(token: string) {
    this.authService.confirmEmail(token).subscribe(param => {
      this.confirmation = true;
      this.message = 'Email is confirmed!';
    }, error => {
      this.confirmation = false;
      this.message = 'Something went wrong, please try again.';
    });
  }

  redirectToLoginPage() {
    this.router.navigate(['main']);
  }

}
