import {Component} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {NgForm} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthService} from '../../../services/auth.service';
import {ChooseUsernameComponent} from '../choose-username/choose-username.component';
import {ModalComponent} from '../../shared/modal/modal.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent {

  title = 'Register';
  closeBtnName: string;
  isLoading = false;
  errorMessage: string;

  constructor(public bsModalRef: BsModalRef,
              public modalService: BsModalService,
              private authService: AuthService) {
  }

  onRegister(form: NgForm) {
    this.isLoading = true;
    if (form.invalid) {
      this.isLoading = false;
      return;
    }
    this.authService.register(form.value.username, form.value.email, form.value.password)
      .pipe(first())
      .subscribe(data => {
        this.isLoading = false;
        this.bsModalRef.hide();
        this.OpenConfirmModal();
      }, error => {
        this.isLoading = false;
        switch (error.status) {
          case 409:
            this.errorMessage = 'Username/email already taken.';
            break;
          default:
            this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
        }
      });
  }

  private OpenConfirmModal() {
    const initialState = {
      title: 'One more thing...',
      content: 'Please confirm your email to activate your account.',
      closeBtnName: 'Ok'
    };
    this.bsModalRef = this.modalService.show(ModalComponent, {initialState});
  }

  openChooseUsernameModal(provider: string) {
    this.bsModalRef.hide();
    this.bsModalRef = this.modalService.show(ChooseUsernameComponent);
    this.bsModalRef.content.provider = provider;
  }
}
