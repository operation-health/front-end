import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChooseUsernameComponent} from './choose-username/choose-username.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {RegisterComponent} from './register/register.component';
import {FormsModule} from '@angular/forms';
import {ModalModule} from 'ngx-bootstrap';
import {AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule} from 'angular-6-social-login';
import {environment} from '../../../environments/environment';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from '../../interceptors/auth-interceptor';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';
import {ConfirmEmailComponent} from './confirm-email/confirm-email.component';

export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(environment.googleApiKey)
      },
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(environment.facebookApiKey)
      }
    ]);
}

@NgModule({
  declarations: [
    ChooseUsernameComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    ConfirmEmailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgxBootstrapModule,
    SocialLoginModule,
    ModalModule.forRoot()
  ], entryComponents: [
    ChooseUsernameComponent,
    ForgotPasswordComponent,
    RegisterComponent
  ], providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs}
  ]
})
export class AuthModule {
}
