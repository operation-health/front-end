import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChooseUsernameComponent} from './choose-username.component';
import {AuthService} from '../../../services/auth.service';
import {BsModalRef} from 'ngx-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {
  AuthService as SocialAuthService,
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
import {environment} from '../../../../environments/environment';
import {HttpClientTestingModule} from '@angular/common/http/testing';

export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(environment.googleApiKey)
      },
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(environment.facebookApiKey)
      }
    ]);
}

describe('ChooseUsernameComponent', () => {
  let component: ChooseUsernameComponent;
  let fixture: ComponentFixture<ChooseUsernameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        SocialAuthService,
        BsModalRef,
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs}
      ],
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ChooseUsernameComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseUsernameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
