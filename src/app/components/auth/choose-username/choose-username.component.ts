import {Component} from '@angular/core';
import {AuthService as SocialAuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular-6-social-login';
import {first} from 'rxjs/operators';
import {AuthService} from '../../../services/auth.service';
import {BsModalRef} from 'ngx-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-choose-username',
  templateUrl: './choose-username.component.html',
  styleUrls: ['./choose-username.component.scss']
})

export class ChooseUsernameComponent {
  public username: string;
  public errorMessage: string;
  private provider: string;


  constructor(private socialAuthService: SocialAuthService,
              private authService: AuthService,
              public bsModalRef: BsModalRef,
              private router: Router) {
  }

  register() {
    this.authService.usernameExists(this.username)
      .subscribe(data => {
        this.registerWithSocial(this.provider);
      }, error => {
        if (error.status === 409) {
          this.errorMessage = 'The player name already exists.';
        } else {
          this.errorMessage = 'Oopsie woopsie, that didn\\\'t work!';
        }
      });
  }

  registerWithSocial(provider: string) {
    let providerId;
    if (provider === 'Google') {
      providerId = GoogleLoginProvider.PROVIDER_ID;
    } else if (provider === 'Facebook') {
      providerId = FacebookLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(providerId)
      .then(userData => {
        this.authService.registerWithSocial(userData, this.username)
          .pipe(first())
          .subscribe(data => {
            this.bsModalRef.hide();
            this.authService.loginWithSocial(userData)
              .pipe(first())
              .subscribe(() => {
                this.router.navigate(['main']);
              }, error => {
                this.errorMessage = error.error;
              });
          }, error => {
            this.errorMessage = 'You seem to already have an account.';
          });
      });
  }

}
