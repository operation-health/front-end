import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {ForgotPasswordComponent} from './forgot-password.component';
import {BsModalRef, ModalModule} from 'ngx-bootstrap';
import {PlayerService} from '../../../services/player.service';
import {FormsModule, NgForm} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  let playerService: PlayerService;
  let debugElement: DebugElement;
  let playerSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        BsModalRef,
        PlayerService
      ],
      imports: [
        FormsModule,
        ModalModule.forRoot(),
        HttpClientTestingModule
      ],
      declarations: [
        ForgotPasswordComponent,
      ]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    debugElement = fixture.debugElement;
    playerService = debugElement.injector.get(PlayerService);
    playerSpy = spyOn(playerService, 'forgotPassword').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Component variable is Loading should be false', () => {
    expect(fixture.componentInstance.isLoading).toBeFalsy();
  });

  it('Form should change IsLoading variable', () => {
    const testForm = <NgForm>{
      value: {
        email: 'test@test.com'
      }
    };
    const comp = fixture.componentInstance;
    comp.onSubmit(testForm);
    expect(fixture.componentInstance.isLoading).toBeTruthy();
  });

  it('should call onLogin method', fakeAsync(() => {
    spyOn(component, 'onSubmit');
    fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);
    expect(component.onSubmit).toHaveBeenCalled();
  }));

  it('Form should call player service', () => {
    const testForm = <NgForm>{
      value: {
        email: 'test@test.com'
      }
    };
    const comp = fixture.componentInstance;
    comp.onSubmit(testForm);
    expect(playerSpy).toHaveBeenCalled();
  });
});
