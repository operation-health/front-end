import {Component} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {NgForm} from '@angular/forms';
import {PlayerService} from '../../../services/player.service';
import {ModalComponent} from '../../shared/modal/modal.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  errorMessage: string;
  isLoading = false;

  constructor(public bsModalRef: BsModalRef,
              public modalService: BsModalService,
              private playerService: PlayerService) {
  }

  onSubmit(form: NgForm) {
    this.isLoading = true;
    if (form.invalid) {
      this.isLoading = false;
      return;
    }

    this.playerService.forgotPassword(form.value.email)
      .subscribe(response => {
        this.errorMessage = null;
        this.bsModalRef.hide();
        this.openConfirmationModal();
      }, error => {
        this.isLoading = false;
        this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
      });
  }

  private openConfirmationModal() {
    const initialState = {
      title: 'Email sent!',
      content: 'Please check your inbox (or spam!).',
      closeBtnName: 'Ok'
    };
    this.bsModalRef = this.modalService.show(ModalComponent, {initialState});
  }
}
