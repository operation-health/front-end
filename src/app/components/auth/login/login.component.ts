import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {first} from 'rxjs/operators';
import {RegisterComponent} from '../register/register.component';
import {AuthService as SocialAuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular-6-social-login';
import {ForgotPasswordComponent} from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessage: string;
  isLoading = false;

  bsModalRef: BsModalRef;
  private config = {
    animated: true
  };

  constructor(private modalService: BsModalService,
              private router: Router,
              private authService: AuthService,
              private socialAuthService: SocialAuthService) {
  }

  ngOnInit(): void {
    if (this.authService.isAuth()) {
      this.router.navigate(['main']);
    }
  }

  onLogin(form: NgForm) {
    this.isLoading = true;
    if (form.invalid) {
      this.isLoading = false;
      return;
    }

    this.authService.login(form.value.name, form.value.password)
      .pipe(first())
      .subscribe(data => {
        this.router.navigate(['main']);
      }, error => {
        this.isLoading = false;
        switch (error.status) {
          case 401:
          case 404:
            this.errorMessage = 'Incorrect username/email or password.';
            break;
          default:
            this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
        }
      });
  }

  openForgotPwModal() {
    this.bsModalRef = this.modalService.show(ForgotPasswordComponent, this.config);
  }

  openRegisterModal() {
    this.bsModalRef = this.modalService.show(RegisterComponent, this.config);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  loginWithFacebook() {
    this.loginWithSocial(FacebookLoginProvider.PROVIDER_ID);
  }

  loginWithGoogle() {
    this.loginWithSocial(GoogleLoginProvider.PROVIDER_ID);
  }

  private loginWithSocial(providerId: string) {
    this.socialAuthService.signIn(providerId).then(userData => {
      this.authService.loginWithSocial(userData).pipe(first()).subscribe(data => {
        this.router.navigate(['main']);
      }, error => {
        this.errorMessage = error.error;
      });
    });
  }
}



