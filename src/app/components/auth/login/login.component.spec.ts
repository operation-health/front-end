import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {FormsModule, NgForm} from '@angular/forms';
import {BsModalRef, BsModalService, ModalModule} from 'ngx-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {By} from '@angular/platform-browser';
import {AuthService} from '../../../services/auth.service';
import {DebugElement} from '@angular/core';
import {AuthService as SocialAuthService, AuthServiceConfig} from 'angular-6-social-login';
import {getAuthServiceConfigs} from '../choose-username/choose-username.component.spec';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let debugElement: DebugElement;
  let loginSpy;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        BsModalService,
        BsModalRef,
        SocialAuthService,
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs}
      ],
      imports: [
        FormsModule,
        ModalModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    debugElement = fixture.debugElement;
    authService = debugElement.injector.get(AuthService);
    loginSpy = spyOn(authService, 'login').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Component variable is Loading should be false', () => {
    expect(fixture.componentInstance.isLoading).toBeFalsy();
  });

  it('#onLogin should change IsLoading variable', () => {
    const testForm = <NgForm>{
      value: {
        username: 'Hello',
        email: 'World',
        password: 'Test123!'
      }
    };
    const comp = fixture.componentInstance;
    comp.onLogin(testForm);
    expect(fixture.componentInstance.isLoading).toBeTruthy();
  });

  it('should call onLogin method', fakeAsync(() => {
    spyOn(component, 'onLogin');
    fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);
    expect(component.onLogin).toHaveBeenCalled();
  }));

  it('#onLogin should call auth service method login', () => {
    const testForm = <NgForm>{
      value: {
        username: 'Hello',
        email: 'World',
        password: 'Test123!'
      }
    };
    const comp = fixture.componentInstance;
    comp.onLogin(testForm);
    expect(loginSpy).toHaveBeenCalled();
  });
});
