import {Component, OnInit} from '@angular/core';
import {NotificationSettings} from '../../../models/notification-settings';
import {NotificationSettingsService} from '../../../services/notification-settings.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notificationSettings: NotificationSettings = new NotificationSettings();
  errorMessage: string;

  constructor(private notificationSettingsService: NotificationSettingsService) {
  }

  ngOnInit() {
    this.notificationSettingsService.getSettings()
      .subscribe(data => {
        this.notificationSettings = data;
      }, error => this.errorMessage = 'Something went wrong while loading your settings.');
  }

  updateSettings() {
    this.notificationSettingsService.changeSettings(this.notificationSettings)
      .subscribe();
  }

}
