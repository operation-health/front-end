import {Component, OnInit} from '@angular/core';
import {Router, RouterOutlet} from '@angular/router';
import {animate, group, query, style, transition, trigger} from '@angular/animations';
import {PlayerService} from '../../../services/player.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    trigger('routeAnimation', [
      transition('1 => *, * => 4, 2 => 3', [
        query(':enter', style({transform: 'translateX(100%)'})),
        query(':enter, :leave', style({position: 'absolute', top: 0, left: 0, width: '100%'})),
        group([
          query(':leave', [animate('0.5s cubic-bezier(.35, 0, .25,  1)', style({transform: 'translateX(-100%)'}))], {optional: true}),
          query(':enter', [animate('0.5s cubic-bezier(.35, 0, .25,  1)', style({transform: 'translateX(0)'}))])
        ])
      ]),
      transition('4 => *, * => 1, 3 => 2', [
        query(':enter', style({transform: 'translateX(-100%)'})),
        query(':enter, :leave', style({position: 'absolute', top: 0, left: 0, width: '100%'})),
        group([
          query(':leave', [animate('0.5s cubic-bezier(.35, 0, .25,  1)', style({transform: 'translateX(100%)'}))], {optional: true}),
          query(':enter', [animate('0.5s cubic-bezier(.35, 0, .25,  1)', style({transform: 'translateX(0)'}))])
        ])
      ])
    ])
  ]
})

export class ProfileComponent implements OnInit {

  constructor(private router: Router,
              private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.playerService.getProfile();
  }

  returnToMenu() {
    this.router.navigate(['/main']);
  }

  getPosition(outlet: RouterOutlet) {
    return outlet.activatedRouteData['position'];
  }
}
