import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlayerService} from '../../../services/player.service';
import {PlayerStats} from '../../../models/profile/player-stats';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit, OnDestroy {

  gamesPlayed = 0;
  gamesWon = 0;

  private profileSub: Subscription;

  constructor(private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.profileSub = this.playerService.getProfileUpdateListener().subscribe(profileData => {
      const playerStats: PlayerStats = this.playerService.getStats();
      this.gamesPlayed = playerStats.gamesPlayed;
      this.gamesWon = playerStats.gamesWon;
    });
  }

  ngOnDestroy(): void {
    this.profileSub.unsubscribe();
  }
}
