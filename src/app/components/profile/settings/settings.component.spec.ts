import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {SettingsComponent} from './settings.component';
import {BsModalService, ModalModule} from 'ngx-bootstrap';
import {PlayerService} from '../../../services/player.service';
import {FormsModule, NgForm} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  let playerService: PlayerService;
  let debugElement: DebugElement;
  let playerSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        BsModalService,
        PlayerService
      ],
      imports: [
        FormsModule,
        HttpClientTestingModule,
        ModalModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [
        SettingsComponent
      ]
    })
      .compileComponents();
    fixture = TestBed.createComponent(SettingsComponent);
    debugElement = fixture.debugElement;
    playerService = debugElement.injector.get(PlayerService);
    playerSpy = spyOn(playerService, 'editAccount').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('isLoading should be false', () => {
    expect(fixture.componentInstance.isLoading).toBeFalsy();
  });

  it('error message should be undefined', () => {
    expect(fixture.componentInstance.errorMessage).toBe(undefined);
  });

  it('success message should be undefined', () => {
    expect(fixture.componentInstance.successMessage).toBe(undefined);
  });

  it('form should change isLoading', () => {
    const testForm = <NgForm>{
      value: {
        email: 'test@test.com'
      }
    };
    const comp = fixture.componentInstance;
    comp.onSubmit(testForm);
    expect(fixture.componentInstance.isLoading).toBeTruthy();
  });

  it('should call onSubmit method', fakeAsync(() => {
    spyOn(component, 'onSubmit');
    fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);
    expect(component.onSubmit).toHaveBeenCalled();
  }));

  it('form should call playerService editAccount', () => {
    const testForm = <NgForm>{
      value: {
        email: 'test@test.com'
      }
    };
    const comp = fixture.componentInstance;
    comp.onSubmit(testForm);
    expect(playerSpy).toHaveBeenCalled();
  });
});
