import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ResetPasswordComponent} from '../reset-password/reset-password.component';
import {NgForm} from '@angular/forms';
import {PlayerService} from '../../../services/player.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  isLoading = false;
  errorMessage: string;
  successMessage: string;
  email: string;

  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService,
              private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.playerService.getEmail()
      .subscribe((response: { email: string }) => {
        this.email = response.email;
      }, error => {
        this.email = ''
      })
  }

  onSubmit(form: NgForm) {
    this.isLoading = true;

    if (form.invalid) {
      this.isLoading = false;
      return;
    }

    this.playerService.editAccount(form.value.email)
      .subscribe(response => {
        this.isLoading = false;
        this.showSuccessMessage();
      }, error => {
        this.isLoading = false;
        this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
      });
  }

  openResetPwModal() {
    const config = {
      animated: true
    };
    this.bsModalRef = this.modalService.show(ResetPasswordComponent, config);
  }

  showSuccessMessage() {
    this.successMessage = 'Email updated successfully!';
    setTimeout(() => {
      this.successMessage = '';
    }, 3000);
  }
}
