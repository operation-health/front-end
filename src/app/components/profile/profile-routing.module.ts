import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../guards/auth.guard';
import {StatsComponent} from './stats/stats.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {SettingsComponent} from './settings/settings.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  {
    path: '', component: ProfileComponent, canActivate: [AuthGuard], children: [
      {path: 'stats', component: StatsComponent, data: {position: 1}},
      {path: 'notifications', component: NotificationsComponent, data: {position: 2}},
      {path: 'privacy', component: PrivacyComponent, data: {position: 3}},
      {path: 'settings', component: SettingsComponent, data: {position: 4}},
      {path: '**', component: StatsComponent}]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ProfileRoutingModule {
}
