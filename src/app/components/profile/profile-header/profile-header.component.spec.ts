import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileHeaderComponent} from './profile-header.component';
import {By} from '@angular/platform-browser';

describe('ProfileHeaderComponent', () => {
  let component: ProfileHeaderComponent;
  let fixture: ComponentFixture<ProfileHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have first list item as \'Stats\'', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement;
    expect(compiled.queryAll(By.css('li'))[0].nativeElement.textContent).toContain('Stats');
  });

  it('should have second list item as \'Notifications\'', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement;
    expect(compiled.queryAll(By.css('li'))[1].nativeElement.textContent).toContain('Notifications');
  });

  it('should have third list item as \'Privacy\'', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement;
    expect(compiled.queryAll(By.css('li'))[2].nativeElement.textContent).toContain('Privacy');
  });

  it('should have fourth list item as \'Settings\'', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement;
    expect(compiled.queryAll(By.css('li'))[3].nativeElement.textContent).toContain('Settings');
  });
});
