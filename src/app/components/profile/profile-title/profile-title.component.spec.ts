import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileTitleComponent} from './profile-title.component';
import {SafeUrlPipe} from '../../../pipes/safe-url.pipe';
import {PlayerService} from '../../../services/player.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ProfileTitleComponent', () => {
  let component: ProfileTitleComponent;
  let fixture: ComponentFixture<ProfileTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        PlayerService
      ],
      imports: [
        HttpClientTestingModule
      ],
      declarations: [
        ProfileTitleComponent,
        SafeUrlPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render h1 tag with \'Profile\'', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Profile');
  });

  it('should have an empty error message', () => {
    expect(fixture.componentInstance.errorMessage).toBeUndefined();
  });

  it('should have an empty image', () => {
    expect(fixture.componentInstance.image).toBeUndefined();
  });
});
