import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../../services/player.service';

@Component({
  selector: 'app-profile-title',
  templateUrl: './profile-title.component.html',
  styleUrls: ['./profile-title.component.scss']
})
export class ProfileTitleComponent implements OnInit {

  errorMessage: string;
  image: any;
  private selectedFiles: FileList;

  constructor(private playerService: PlayerService) {
  }

  ngOnInit() {
    this.getAvatar();

  }

  selectFile(event) {
    this.errorMessage = '';
    const file = event.target.files.item(0);

    if (file.type.match('image.*')) {
      this.errorMessage = '';
      this.selectedFiles = event.target.files;
      this.uploadAvatar();
    } else {
      this.errorMessage = 'Invalid file format.';
    }
  }

  uploadAvatar() {
    this.playerService.uploadAvatar(this.selectedFiles.item(0))
      .subscribe(response => {
        this.createImageFromBlob(this.selectedFiles.item(0));
      }, error => {
        switch (error.error.status) {
          case 500:
            this.errorMessage = 'File too large';
            break;
          default:
            this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
        }
      });
  }

  getAvatar() {
    this.playerService.getAvatar()
      .subscribe(response => {
        this.createImageFromBlob(response);
      }, error => {
        this.image = '../../../../assets/img/flag.png';
      });
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.image = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
