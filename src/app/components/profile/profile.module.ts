import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile/profile.component';
import {ProfileHeaderComponent} from './profile-header/profile-header.component';
import {ProfileTitleComponent} from './profile-title/profile-title.component';
import {StatsComponent} from './stats/stats.component';
import {SettingsComponent} from './settings/settings.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {SafeUrlPipe} from '../../pipes/safe-url.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalModule} from 'ngx-bootstrap';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileTitleComponent,
    ProfileHeaderComponent,
    StatsComponent,
    NotificationsComponent,
    PrivacyComponent,
    SettingsComponent,
    ResetPasswordComponent,
    SafeUrlPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxBootstrapModule,
    ModalModule.forRoot(),
    ProfileRoutingModule
  ],
  entryComponents: [
    ResetPasswordComponent
  ]
})
export class ProfileModule {
}
