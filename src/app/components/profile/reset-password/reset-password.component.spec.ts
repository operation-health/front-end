import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ResetPasswordComponent} from './reset-password.component';
import {BsModalRef, ModalModule} from 'ngx-bootstrap';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PlayerService} from '../../../services/player.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        BsModalRef,
        FormBuilder,
        PlayerService],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        ModalModule.forRoot(),
        FormsModule
      ],
      declarations: [ResetPasswordComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('isLoading should be false', () => {
    expect(fixture.componentInstance.isLoading).toBeFalsy();
  });

  it('error message should be undefined', () => {
    expect(fixture.componentInstance.errorMessage).toBe(undefined);
  });

  it('success message should be undefined', () => {
    expect(fixture.componentInstance.successMessage).toBe(undefined);
  });
});
