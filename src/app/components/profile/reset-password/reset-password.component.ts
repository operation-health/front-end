import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PlayerService} from '../../../services/player.service';
import {ForgotPasswordComponent} from '../../auth/forgot-password/forgot-password.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  errorMessage: string;
  successMessage: string;
  isLoading = false;

  resetPwForm: FormGroup;

  constructor(public bsModalRef: BsModalRef,
              private formBuilder: FormBuilder,
              private playerService: PlayerService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.resetPwForm = this.formBuilder.group({
      currentPw: ['', [Validators.required]],
      newPw: ['', [Validators.required]],
      confirmPw: ['', [Validators.required]],
    }, {validator: this.checkPasswords});
  }

  onSubmit() {
    this.isLoading = true;
    this.errorMessage = '';

    if (this.resetPwForm.invalid) {
      this.errorMessage = 'Password don\'t match';
      this.isLoading = false;
      return;
    }

    this.playerService.editAccount(undefined, this.resetPwForm.value.newPw)
      .subscribe(response => {
        this.isLoading = false;
        this.showSuccessMessage();
      }, error => {
        this.isLoading = false;
        this.errorMessage = 'Oopsie woopsie, that didn\'t work! Please try again.';
      });
  }

  checkPasswords(group: FormGroup) {
    const pass = group.controls.newPw.value;
    const confirmPass = group.controls.confirmPw.value;

    return pass === confirmPass ? null : {notSame: true};
  }

  showSuccessMessage() {
    this.successMessage = 'Password updated successfully!';
    setTimeout(() => {
      this.successMessage = '';
      this.bsModalRef.hide();
    }, 3000);
  }

  openForgotPwModal() {
    const config = {
      animated: true
    };
    this.bsModalRef.hide();
    this.bsModalRef = this.modalService.show(ForgotPasswordComponent, config);
  }

}
