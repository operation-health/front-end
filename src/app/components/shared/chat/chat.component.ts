import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {GameMessage} from '../../../models/game-message';
import {ChatService} from '../../../services/chat.service';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AuthService} from '../../../services/auth.service';
import {StreamService} from '../../../services/stream.service';
import Action from '../../../models/event/action';
import ChatGameMessageReceived from '../../../models/event/actions/chat/chat-game-message-received';
import ActionEvent from '../../../models/event/action-event';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

  gameMessages: GameMessage[] = [];
  @Input()
  closeTab: () => void;
  @Input()
  openTab: () => void;

  playerName: string;

  private streamSub: Subscription;

  constructor(private chatService: ChatService,
              private authService: AuthService,
              private streamService: StreamService) {
  }

  ngOnInit() {
    this.playerName = this.authService.getPlayerName();
    this.streamSub = this.streamService.getStream().subscribe((event: ActionEvent) => {
      switch (event.action) {
        case Action.CHAT_GAME_MESSAGE_RECEIVED: {
          const payload = <ChatGameMessageReceived>event.payload;
          this.gameMessages.push(payload);
          this.openTab();
          break;
        }
      }
    });
  }

  ngOnDestroy() {
    this.streamSub.unsubscribe();
  }

  onMessageSend(form: NgForm) {
    if (form.invalid) {
      return;
    }

    const gameMessage: GameMessage = {
      message: form.value.message
    };
    form.resetForm();
    this.chatService.sendMessage(gameMessage);
  }
}
