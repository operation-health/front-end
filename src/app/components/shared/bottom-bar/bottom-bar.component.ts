import {Component, HostListener} from '@angular/core';
import {FriendsComponent} from '../friends/friends.component';
import {ActivationStart, NavigationStart, Router} from '@angular/router';
import {ChatComponent} from '../chat/chat.component';
import {Tab, TabType} from '../../../models/tab';
import {AuthService} from '../../../services/auth.service';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss']
})
export class BottomBarComponent {

  private TAB_WIDTH_IN_PX_LG = 300;
  private TAB_WIDTH_IN_PX_MD = 250;
  private TAB_WIDTH_IN_PX_SM = 150;
  private TAB_WIDTH_OFFSET = 5;

  isAuth = false;
  tabOffset = this.TAB_WIDTH_IN_PX_LG + this.TAB_WIDTH_OFFSET;
  tabWidth = this.TAB_WIDTH_IN_PX_LG;

  tabs: Tab[] = [{
    label: 'Friends',
    type: TabType.FRIENDS,
    component: FriendsComponent
  }, {
    label: 'Game chat',
    type: TabType.CHAT_GAME,
    component: ChatComponent,
    disabled: true
  }];

  constructor(private router: Router, private authService: AuthService) {
    this.handleResize(window.innerWidth);
    this.router.events.pipe(
      filter(event => event instanceof ActivationStart)
    ).subscribe(event => {
      const enableChatGame = event['snapshot'].data.tabs ? event['snapshot'].data.tabs[TabType.CHAT_GAME] : false;
      this.tabs.find(tab => tab.type === TabType.CHAT_GAME).disabled = !enableChatGame;
    });
    router.events.subscribe(route => {
      if (route instanceof NavigationStart) {
        this.isAuth = this.authService.isAuth();
      }
    });
  }

  toggleTabOpen(label: string) {
    const tab = this.tabs.find(t => t.label === label);
    tab.open = !tab.open;
  }

  openTab(label: string) {
    this.tabs.find(t => t.label === label).open = true;
  }

  closeTab(label: string) {
    this.tabs.find(t => t.label === label).open = false;
  }

  createInputs(tab: Tab) {
    return {
      openTab: () => this.openTab(tab.label), // These get injected into the tab components
      closeTab: () => this.closeTab(tab.label)
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.handleResize(window.innerWidth);
  }

  handleResize(windowWidth: number) {
    if (windowWidth > 992) this.changeTabWidth(this.TAB_WIDTH_IN_PX_LG, this.TAB_WIDTH_OFFSET);
    if (windowWidth < 768) this.changeTabWidth(this.TAB_WIDTH_IN_PX_MD, this.TAB_WIDTH_OFFSET);
    if (windowWidth < 576) this.changeTabWidth(this.TAB_WIDTH_IN_PX_SM, this.TAB_WIDTH_OFFSET);
  }

  changeTabWidth(tabWidth: number, tabOffset: number) {
    this.tabWidth = tabWidth;
    this.tabOffset = tabWidth + tabOffset;
  }
}
