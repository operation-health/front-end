import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BottomBarComponent} from './bottom-bar.component';
import {FormsModule} from '@angular/forms';
import {ChatComponent} from '../chat/chat.component';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FriendsComponent} from '../friends/friends.component';
import {BsModalService, ModalModule} from 'ngx-bootstrap';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DynamicModule} from 'ng-dynamic-component';

describe('BottomBarComponent', () => {
  let component: BottomBarComponent;
  let fixture: ComponentFixture<BottomBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ModalModule.forRoot(),
        DynamicModule
      ],
      providers: [
        BsModalService
      ],
      declarations: [
        BottomBarComponent,
        ChatComponent,
        FriendsComponent
      ]
    }).compileComponents();
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ChatComponent, FriendsComponent]
      }
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
