import {Component} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {NgForm} from '@angular/forms';
import {PlayerService} from '../../../services/player.service';
import {Friend} from '../../../models/lobby/friend';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.scss']
})
export class AddFriendComponent {

  searchResult: Friend[] = [];
  friends: Friend[] = [];
  Test: Friend[] = [];
  searched = false;

  constructor(public bsModalRef: BsModalRef,
              private playerService: PlayerService) {
  }

  onSearchFriend(form: NgForm) {
    this.playerService.searchFriend(form.value.searchValue).subscribe((response: any) => {
      this.searchResult = response;
      this.searched = true;
    });
  }

  addFriend(playerName: string) {
    this.playerService.addFriend(playerName).subscribe();
    this.bsModalRef.hide();
  }

  removeFriend(playerName: string) {
    this.playerService.removeFriend(playerName).subscribe();
  }

  isFriend(playerName: string): boolean {
    return this.friends.map(friend => friend.playerName).includes(playerName);
  }
}
