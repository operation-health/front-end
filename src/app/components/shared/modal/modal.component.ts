import {Component} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-component',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  title: string;
  content: string;
  closeBtnName: string;

  constructor(public bsModalRef: BsModalRef) {
  }
}
