import {NgModule} from '@angular/core';
import {BottomBarComponent} from './bottom-bar/bottom-bar.component';
import {ChatComponent} from './chat/chat.component';
import {FriendsComponent} from './friends/friends.component';
import {ModalModule} from 'ngx-bootstrap';
import {ModalComponent} from './modal/modal.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgxBootstrapModule} from '../../ngx-bootstrap.module';
import {LobbyStatsComponent} from './lobby-stats/lobby-stats.component';
import {SafeUrlPipe} from '../../pipes/safe-url.pipe';
import {DynamicModule} from 'ng-dynamic-component';
import {GameHeaderComponent} from './game/game-header/game-header.component';
import {FormatTimePipe} from '../../pipes/format-time.pipe';
import {BoardComponent} from './game/board/board.component';
import {CardComponent} from './game/card/card.component';
import {AttackCardsComponent} from './game/attackcards/attack-cards.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {AddFriendComponent} from './add-friend/add-friend.component';

@NgModule({
  declarations: [
    BottomBarComponent,
    ChatComponent,
    FriendsComponent,
    AddFriendComponent,
    ModalComponent,
    GameHeaderComponent,
    FormatTimePipe,
    BoardComponent,
    CardComponent,
    AttackCardsComponent,
    LobbyStatsComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgxBootstrapModule,
    ModalModule.forRoot(),
    DynamicModule.withComponents(
      [ChatComponent, FriendsComponent]
    ),
    InlineSVGModule.forRoot()
  ],
  exports: [
    BottomBarComponent,
    GameHeaderComponent,
    BoardComponent,
    CardComponent,
    AttackCardsComponent
  ],
  providers: [
    SafeUrlPipe
  ],
  entryComponents: [
    ChatComponent,
    FriendsComponent,
    AddFriendComponent,
    LobbyStatsComponent,
    ModalComponent
  ]
})
export class SharedModule {
}
