import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../../services/player.service';
import {SafeUrlPipe} from '../../../pipes/safe-url.pipe';
import {BsModalRef} from 'ngx-bootstrap';
import {PlayerStats} from '../../../models/profile/player-stats';

@Component({
  selector: 'app-lobby-stats',
  templateUrl: './lobby-stats.component.html',
  styleUrls: ['./lobby-stats.component.scss']
})
export class LobbyStatsComponent implements OnInit {

  title = 'Stats';
  playerName: string;
  profilePicture: any;
  playerStats: PlayerStats = new PlayerStats();
  success: boolean;
  errorMessage: string;

  constructor(private playerService: PlayerService,
              private safeUrl: SafeUrlPipe,
              public bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
    this.getAvatar();
    this.loadStats();
  }

  getAvatar() {
    this.playerService.getAvatarFromOtherPlayer(this.playerName)
      .subscribe(response => {
        this.createImageFromBlob(response);
      }, error => {
        this.profilePicture = '../../../../assets/img/flag.png';
      });
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.profilePicture = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  loadStats() {
    this.playerService.getStatsOfPlayer(this.playerName).subscribe((response: PlayerStats) => {
        this.playerStats = response;
        this.success = true;
        this.errorMessage = '';
      },
      error => {
        this.success = false;
        this.errorMessage = this.playerName + ' does not show their stats publicly.';
      }
    );
  }

  transformImage() {
    return this.safeUrl.transform(this.profilePicture);
  }
}
