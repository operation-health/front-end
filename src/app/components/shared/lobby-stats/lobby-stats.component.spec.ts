import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LobbyStatsComponent} from './lobby-stats.component';
import {SafeUrlPipe} from '../../../pipes/safe-url.pipe';
import {HttpClientModule} from '@angular/common/http';
import {BsModalRef} from 'ngx-bootstrap';

describe('LobbyStatsComponent', () => {
  let component: LobbyStatsComponent;
  let fixture: ComponentFixture<LobbyStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        SafeUrlPipe,
        BsModalRef
      ],
      declarations: [
        LobbyStatsComponent,
        SafeUrlPipe
      ],
      imports: [
        HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
