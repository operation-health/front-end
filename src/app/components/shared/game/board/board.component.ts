import {AfterViewChecked, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Board} from '../../../../models/board/board';
import {BoardService} from '../../../../services/board.service';
import * as SVG from 'svg.js';
import 'svg.draggable.js';
import 'svg.panzoom.js';
import Rank, {getOrder} from '../../../../models/board/rank';
import DragController from './drag-controller';
import Team from '../../../../models/game/team';
import GamePlayer from '../../../../models/game/gameplayer';
import {AuthService} from '../../../../services/auth.service';
import {Observable} from 'rxjs';
import Action from '../../../../models/event/action';
import GameState from '../../../../models/game/gamestate';
import ActionEvent from '../../../../models/event/action-event';
import GamePawnOrganised from '../../../../models/event/actions/game/game-pawn-organised';
import GameFlagKilled from '../../../../models/event/actions/game/game-flag-killed';
import GamePawnAttacked from '../../../../models/event/actions/game/game-pawn-attacked';
import GameFinalBlow from '../../../../models/event/actions/game/game-final-blow';
import GamePawnMoved from '../../../../models/event/actions/game/game-pawn-moved';
import GameTransitionToPlaying from '../../../../models/event/actions/game/game-transition-to-playing';
import Game from '../../../../models/game/game';
import GameDoubleElimination from '../../../../models/event/actions/game/game-double-elimination';
import GamePassiveWin from '../../../../models/event/actions/game/game-passive-win';
import GameHumansEliminated from '../../../../models/event/actions/game/game-humans-eliminated';
import GameAttritionKill from '../../../../models/event/actions/game/game-attrition-kill';
import GameAttritionVictory from '../../../../models/event/actions/game/game-attrition-victory';
import MovePawn from '../../../../models/board/move-pawn';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})

export class BoardComponent implements OnInit, AfterViewChecked, OnDestroy {

  gameState: GameState;
  gamePlayers: GamePlayer[];
  board: Board;
  boardSVGs = {
    WATERLOO: 'assets/img/boards/waterloo.svg',
    PASSENDALE: 'assets/img/boards/passendale.svg'
  };
  backgrounds = [
    'assets/img/boards/beach-map.jpg',
    'assets/img/boards/grass-map.jpg',
    'assets/img/boards/rocky-map.jpg'
  ];
  backgroundImage = this.backgrounds[0];

  @Input() type: string;
  @Input() actionEventObservable: Observable<ActionEvent>;
  @Input() gameObservable: Observable<Game>;
  @Output() move: EventEmitter<MovePawn> = new EventEmitter<MovePawn>();

  private gameEventsSub;
  private gameSub;
  private initialised = false;
  private dragController: DragController;

  private teamColors = {
    [Team.TEAM1]: 'blue',
    [Team.TEAM2]: 'red',
    [Team.TEAM3]: 'yellow',
    [Team.TEAM4]: 'green'
  };

  constructor(private boardService: BoardService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.backgroundImage = this.backgrounds[Math.trunc(Math.random() * this.backgrounds.length)];

    if (this.type) {
      this.boardService.getReplayBoard(this.type).subscribe((board: Board) => {
        this.board = board;
      });
    } else {
      this.boardService.getBoard().subscribe((board: Board) => {
        this.board = board;
      });
    }

    if (this.actionEventObservable) {
      this.subscribeToActionEvent();
    }

    if (this.gameObservable) {
      this.subscribeToGame();
    }
  }

  subscribeToActionEvent() {
    this.gameEventsSub = this.actionEventObservable.subscribe(actionEvent => {

      switch (actionEvent.action) {
        case Action.GAME_PAWN_ORGANISED: {
          const payload = <GamePawnOrganised>actionEvent.payload;
          this.dragController.movePawnIdToTileId(payload.pawnId, payload.tileId);
          break;
        }
        case Action.GAME_PAWN_MOVED: {
          const payload = <GamePawnMoved>actionEvent.payload;
          this.dragController.movePawnIdToTileId(payload.pawnId, payload.tileId);
          break;
        }
        case Action.GAME_PAWN_ATTACKED: {
          const payload = <GamePawnAttacked>actionEvent.payload;
          if (!payload.movedPawnDead) {
            this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
          }
          if (payload.movedPawnDead) {
            this.dragController.clearPawnByPawnId(payload.movedPawnId);
          }
          if (payload.attackedPawnDead) {
            this.dragController.clearPawnByPawnId(payload.attackedPawnId);
          }
          break;
        }
        case Action.GAME_DOUBLE_ELIMINATION: {
          const payload = <GameDoubleElimination>actionEvent.payload;
          this.dragController.clearPawnsByTeam(payload.currentTeam);
          this.dragController.clearPawnsByTeam(payload.attackedTeam);
          break;
        }
        case Action.GAME_FLAG_KILLED: {
          const payload = <GameFlagKilled>actionEvent.payload;
          this.dragController.clearPawnsByTeam(payload.attackedTeam);
          this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
          break;
        }
        case Action.GAME_PASSIVE_WIN: {
          const payload = <GamePassiveWin>actionEvent.payload;
          this.dragController.clearPawnsByTeam(payload.currentTeam);
          this.dragController.clearPawnsByTeam(payload.attackedTeam);
          break;
        }
        case Action.GAME_HUMANS_ELIMINATED: {
          const payload = <GameHumansEliminated>actionEvent.payload;
          if (payload.currentTeamDead) {
            this.dragController.clearPawnsByTeam(payload.currentTeam);
          } else {
            this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
          }
          if (payload.attackedTeamDead) {
            this.dragController.clearPawnsByTeam(payload.attackedTeam);
          }
          break;
        }
        case Action.GAME_ATTRITION_KILL: {
          const payload = <GameAttritionKill>actionEvent.payload;
          if (payload.currentTeamDead) {
            this.dragController.clearPawnsByTeam(payload.currentTeam);
          } else {
            this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
          }
          if (!payload.currentTeamDead) {
            this.dragController.clearPawnsByTeam(payload.attackedTeam);
          }
          break;
        }
        case Action.GAME_ATTRITION_VICTORY: {
          const payload = <GameAttritionVictory>actionEvent.payload;
          if (payload.currentTeamDead) {
            this.dragController.clearPawnsByTeam(payload.currentTeam);
          } else {
            this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
            this.dragController.clearPawnsByTeam(payload.attackedTeam);
          }
          break;
        }
        case Action.GAME_FINAL_BLOW: {
          const payload = <GameFinalBlow>actionEvent.payload;
          this.dragController.clearPawnsByTeam(payload.attackedTeam);
          this.dragController.movePawnIdToTileId(payload.movedPawnId, payload.tileId);
          break;
        }
        case Action.GAME_TRANSITION_TO_PLAYING: {
          const payload = <GameTransitionToPlaying>actionEvent.payload;
          this.moveBox(1000);
          this.gamePlayers = payload.game.gamePlayers;
          this.gameState = payload.game.gameState;

          this.dragController.clearPawns();
          this.updatePawns();
          break;
        }
      }
    });
  }

  subscribeToGame() {
    this.gameSub = this.gameObservable.subscribe((game: Game) => {
      this.gamePlayers = game.gamePlayers;
      this.gameState = game.gameState;

      if (this.dragController) {
        this.dragController.clearPawns();
        this.updatePawns();
      }
    });
  }

  ngOnDestroy() {
    this.gameEventsSub.unsubscribe();
    this.gameSub.unsubscribe();
  }

  ngAfterViewChecked() {
    if (this.board === undefined || this.initialised) {
      return;
    }
    const boardsvg = document.getElementById('boardsvg');
    if (boardsvg == null) {
      return;
    }
    this.initialised = true;

    const draw = SVG(boardsvg) as any;
    draw.panZoom({
      zoomMin: 0.25,
      zoomMax: 3
    });
    const boxContainer = draw.select('#boxlayer').get(0);
    const boardElements = draw.select('.boardtile').valueOf();
    const boxElements = draw.select('.boxtile').valueOf();
    const areaTeams = draw.select('.areateam').valueOf();
    areaTeams.forEach(areaTeam => {
      areaTeam.fill(this.teamColors[areaTeam.id().replace('area-team', 'TEAM')]).opacity(0.3);
    });

    const gamePlayer = this.getOwnGamePlayer();

    this.dragController = new DragController(
      draw,
      boxContainer,
      this.board.boardTiles,
      boardElements,
      boxElements,
      gamePlayer.team,
      () => this.gameState === GameState.PREPARING || (this.gameState === GameState.PLAYING && this.isCurrentTurn()),
      () => this.gameState === GameState.PREPARING,
      movePawn => this.move.emit(movePawn)
    );

    this.updatePawns();

    if (this.gameState === GameState.PLAYING) {
      this.moveBox(1000);
    }
  }

  private updatePawns() {
    const draw = SVG(document.getElementById('boardsvg'));
    const gamePlayer = this.getOwnGamePlayer();
    const otherGamePlayers = this.gamePlayers.filter(gp => gp.team !== gamePlayer.team);

    this.setOwnPawns(draw, gamePlayer);
    otherGamePlayers.forEach(gp => this.setOtherPawns(draw, gp));
  }

  private setOwnPawns(draw: SVG.Container, gamePlayer: GamePlayer) {
    if (gamePlayer.dead) {
      return;
    }

    gamePlayer.pawns
      .sort((pawn1, pawn2) => getOrder(pawn1.rank) - getOrder(pawn2.rank))
      .forEach(pawn => {
        if (pawn.dead) {
          return;
        }
        const pawnElement = this.createPawn(draw, pawn.rank, gamePlayer.team);
        this.dragController.registerPawn(pawn, gamePlayer.team, pawnElement);

        if (pawn.boardTile) {
          this.dragController.movePawnElementToElement(pawnElement, this.dragController.getBoardElement(pawn.boardTile.tileId));
        } else {
          this.dragController.placePawnElementInBox(pawnElement);
        }
      });
  }

  private setOtherPawns(draw: SVG.Container, gamePlayer: GamePlayer) {
    if (gamePlayer.dead) {
      return;
    }
    gamePlayer.pawns.forEach(pawn => {
      if (pawn.dead) {
        return;
      }
      const pawnElement = this.createPawn(draw, pawn.rank, gamePlayer.team);
      this.dragController.registerPawn(pawn, gamePlayer.team, pawnElement);

      if (pawn.boardTile) {
        this.dragController.movePawnElementToElement(pawnElement, this.dragController.getBoardElement(pawn.boardTile.tileId));
      } else {
        pawnElement.opacity(0);
      }
    });
  }

  createPawn(draw: SVG.Container, rank: Rank, team: Team) {
    return draw.image(this.getPawnUrl(rank, team), 35, 47);
  }

  private getPawnUrl(rank: Rank, team: Team) {
    const t = this.teamColors[team];
    if (!rank) {
      return `assets/img/ranks/${t}/${t}-tower.svg`;
    }
    const r = rank.toString().toLowerCase();
    return `assets/img/ranks/${t}/${t}-${r}.svg`;
  }

  moveBox(duration: number) {
    this.dragController.moveBox(duration);
  };

  private isCurrentTurn() {
    return this.getOwnGamePlayer().currentTurn;
  }

  private getOwnGamePlayer() {
    return this.gamePlayers.filter(gp => !gp.computer)
      .find(gp => gp.player.playerName === this.authService.getPlayerName());
  }

  isInPlayingPhase() {
    return this.gameState === GameState.PLAYING;
  }
}
