import * as SVG from 'svg.js';
import {canMove} from '../../../../models/board/rank';
import {BoardTile} from '../../../../models/board/boardTile';
import Pawn from '../../../../models/game/pawn';
import Team from '../../../../models/game/team';
import MovePawn from '../../../../models/board/move-pawn';

interface DragHandler {
  pawnElement: SVG.Element;
  startElement: SVG.Element;
  lastElement: SVG.Element;
  allowed: SVG.Element[];
}

interface TileData {
  boardTile: BoardTile;
  pawnElement: SVG.Element;
}

interface PawnData {
  team: Team;
  pawnId: number;
  startTileElement: SVG.Element;
}

export default class DragController {

  private readonly boxContainerElement: SVG.Element; // The whole box
  private boardElements: SVG.Element[];
  private boxElements: SVG.Element[];
  private pawnElements: SVG.Element[] = [];

  private readonly draw: SVG.Container;
  private readonly dragAllowed: () => boolean;
  private readonly boxAllowed: () => boolean;
  private readonly onDrag: (movePawn: MovePawn) => void;
  private readonly team: Team;

  private color = '#0f0';
  private errorColor = '#f00';
  private startColor = '#e8e8e8';
  private boxColor = '#6f2800';

  private opacity = 0.6;
  private emphasisOpacity = 0.9;

  constructor(draw: SVG.Container, boxContainerElement: SVG.Element, boardTiles: BoardTile[], boardElements, boxElements, team, dragAllowed, boxAllowed, onDrag) {
    this.draw = draw;
    this.boxContainerElement = boxContainerElement;
    this.boardElements = boardElements;
    this.boxElements = boxElements;
    this.team = team;
    this.dragAllowed = dragAllowed;
    this.boxAllowed = boxAllowed;
    this.onDrag = onDrag;

    this.boxElements.forEach(boxElement => {
      boxElement.fill(this.boxColor).opacity(1);
      boxElement.remember('data', {
        pawnElement: undefined
      } as TileData);
    });
    this.boardElements.forEach(tileElement => {
      tileElement.opacity(0);
      tileElement.remember('data', {
        boardTile: boardTiles.find(bt => bt.tileId === +tileElement.id().replace('boardtile', '')),
        pawnElement: undefined
      } as TileData);
    });
  }

  registerPawn(pawn: Pawn, team: Team, pawnElement) {
    pawnElement.remember('data', {
      team: team,
      pawnId: pawn.id
    } as PawnData);
    this.pawnElements.push(pawnElement);

    if (this.team === team) {
      pawnElement.draggable();
      pawnElement.on('beforedrag', this.handleBeforeDrag(pawn, pawnElement));
      pawnElement.on('dragstart', this.handleDragStart);
      pawnElement.on('dragmove', this.handleDragMove);
      pawnElement.on('dragend', this.handleDragEnd);
    }
  }

  private handleBeforeDrag = (pawn: Pawn, pawnElement: SVG.Element) => event => {
    if (!this.dragAllowed()) {
      event.preventDefault();
      return;
    }
    if (!this.boxAllowed() && !canMove(pawn.rank)) {
      event.preventDefault();
      return;
    }

    const pawnData: PawnData = pawnElement.remember('data');
    const startTileData: TileData = pawnData.startTileElement.remember('data');

    const allowed = (
      (this.boxAllowed()) ?
        this.boxElements.concat(this.boardElements.filter(boardElement => boardElement.hasClass(this.team.toString().toLowerCase()))) :
        startTileData.boardTile.connectedTileIds.map(tileId => this.getBoardElement(tileId))
    )
      .filter(e => {
        const tileData: TileData = e.remember('data');
        if (!tileData.pawnElement) {
          return true;
        }

        const team: Team = tileData.pawnElement.remember('data').team;
        return team !== pawnData.team;
      });

    event.detail.handler.dragHandler = {
      pawnElement,
      startElement: pawnData.startTileElement,
      allowed
    } as DragHandler;
  };

  private handleDragStart = event => {
    if (!this.dragAllowed || !event.detail.handler) {
      return;
    }
    const dragHandler: DragHandler = event.detail.handler.dragHandler;
    dragHandler.allowed.forEach(tile => tile.fill(this.color).opacity(this.opacity));
    dragHandler.startElement.fill(this.startColor).opacity(this.emphasisOpacity);
  };

  private handleDragMove = event => {
    if (!this.dragAllowed || !event.detail.handler) {
      return;
    }
    const dragHandler: DragHandler = event.detail.handler.dragHandler;
    this.clearLastHoverTile(dragHandler);

    const boardElement = this.getBoardElementByEvent(event);
    if (boardElement) {
      if (dragHandler.startElement === boardElement) {
        boardElement.fill(this.startColor).opacity(this.emphasisOpacity);
      } else if (dragHandler.allowed.includes(boardElement)) {
        boardElement.fill(this.color).opacity(this.emphasisOpacity);
      } else {
        boardElement.fill(this.errorColor).opacity(this.emphasisOpacity);
      }
      dragHandler.lastElement = boardElement;
      return;
    }

    const boxElement = this.getBoxElementByEvent(event);
    if (boxElement) {
      if (!this.boxAllowed()) {
        boxElement.fill(this.errorColor).opacity(this.emphasisOpacity);
      } else if (dragHandler.startElement === boxElement) {
        boxElement.fill(this.startColor).opacity(this.emphasisOpacity);
      } else {
        boxElement.fill(this.color).opacity(this.emphasisOpacity);
      }
      dragHandler.lastElement = boxElement;
    }
  };

  private handleDragEnd = event => {
    if (!this.dragAllowed || !event.detail.handler) {
      return;
    }
    const dragHandler: DragHandler = event.detail.handler.dragHandler;
    const pawnData: PawnData = dragHandler.pawnElement.remember('data');

    this.clearLastHoverTile(dragHandler);
    dragHandler.startElement.opacity(0);
    dragHandler.allowed.forEach(tile => tile.opacity(0));
    this.boxElements.forEach(element => element.fill(this.boxColor).opacity(1));

    if (!this.dragAllowed) {
      this.movePawnElementToElement(dragHandler.pawnElement, dragHandler.startElement);
      return;
    }

    const boardElement = this.getBoardElementByEvent(event);
    if (boardElement && dragHandler.allowed.includes(boardElement)) {
      const tileData: TileData = boardElement.remember('data');
      const tileId = tileData.boardTile.tileId;

      const movePawn = {
        id: pawnData.pawnId,
        tileId: tileId
      };
      this.onDrag(movePawn);
      this.movePawnElementToElement(dragHandler.pawnElement, boardElement);
      return;
    }

    const boxElement = this.getBoxElementByEvent(event);
    if (boxElement && dragHandler.allowed.includes(boxElement) && this.boxAllowed()) {
      const movePawn = {
        id: pawnData.pawnId,
        tileId: -1
      };
      this.onDrag(movePawn);
      this.movePawnElementToElement(dragHandler.pawnElement, boxElement);
      return;
    }

    this.movePawnElementToElement(dragHandler.pawnElement, dragHandler.startElement);
  };

  getBoardElement = (tileId: number) => this.boardElements.find(et => et.id() === 'boardtile' + tileId);
  getBoardElementByEvent = event => this.boardElements.find(element => element.id() === this._getBoardTileIdByEvent(event));
  _getBoardTileIdByEvent = event => {
    const {x, y} = this.getCoordsByEvent(event);
    const domElement = document.elementsFromPoint(x, y).find(e => e.classList.contains('boardtile'));
    return domElement ? domElement.id : undefined;
  };

  getBoxElement = (id: number) => this.boxElements.find(element => element.id() === 'boxtile' + id);
  getBoxElementByEvent = event => this.boxElements.find(element => element.id() === this._getBoxTileIdByEvent(event));
  _getBoxTileIdByEvent = event => {
    const {x, y} = this.getCoordsByEvent(event);
    const domElement = document.elementsFromPoint(x, y).find(e => e.classList.contains('boxtile'));
    return domElement ? domElement.id : undefined;
  };

  private getCoordsByEvent = event => {
    if (event.detail.event.clientX) {
      const {clientX, clientY} = event.detail.event;
      return {
        x: clientX,
        y: clientY
      };
    } else {
      const {clientX, clientY} = event.detail.event.changedTouches[0];
      return {
        x: clientX,
        y: clientY
      };
    }
  };

  clearLastHoverTile = (dragHandler: DragHandler) => {
    const element = dragHandler.lastElement;
    if (element === undefined) {
      return;
    }

    if (dragHandler.startElement === element) {
      dragHandler.lastElement.fill(this.startColor).opacity(this.opacity);
    } else if (dragHandler.allowed.includes(element)) {
      element.fill(this.color).opacity(this.opacity);
    } else if (element.hasClass('boxtile')) {
      element.fill(this.boxColor).opacity(1);
    } else {
      element.opacity(0);
    }
  };

  clearPawns() {
    this.pawnElements.forEach(pawnElement => {
      const pawnData: PawnData = pawnElement.remember('data');
      pawnElement.remove();
      if (pawnData.startTileElement) {
        const tileData: TileData = pawnData.startTileElement.remember('data');
        tileData.pawnElement = undefined;
      }
    });
    this.pawnElements = [];
  }

  clearPawnByPawnId(pawnId: number) {
    const pawnElement = this.pawnElements.find(pe => pe.remember('data').pawnId === pawnId);
    this.removePawn(pawnElement);
  }

  clearPawnsByTeam(team: Team) {
    const pawnElements = this.pawnElements.filter(pawnElement => pawnElement.remember('data').team === team);
    pawnElements.forEach(pawnElement => this.removePawn(pawnElement));
  }

  private removePawn(pawnElement: SVG.Element) {
    const pawnData: PawnData = pawnElement.remember('data');
    pawnElement.opacity(0);
    pawnElement.center(0, 0);
    if (pawnData.startTileElement) {
      const tileData: TileData = pawnData.startTileElement.remember('data');
      tileData.pawnElement = undefined;
    }
  }

  movePawnIdToTileId(pawnId: number, tileId: number) {
    const pawnElement = this.pawnElements.find(pe => pe.remember('data').pawnId === pawnId);
    if (tileId === -1) {
      const pawnData: PawnData = pawnElement.remember('data');
      if (pawnData.team !== this.team) {
        this.clearPawnByPawnId(pawnId);
      }
      return;
    }

    const tileElement = this.getBoardElement(tileId);
    this.movePawnElementToElement(pawnElement, tileElement);
  }

  movePawnElementToElement(pawnElement: SVG.Element, tileElement: SVG.Element) {
    const rbox = tileElement.rbox(this.draw);
    pawnElement.center(rbox.cx, rbox.cy);
    pawnElement.opacity(1);

    const pawnData: PawnData = pawnElement.remember('data');
    const tileData: TileData = tileElement.remember('data');

    if (pawnData.startTileElement) {
      const startTileData: TileData = pawnData.startTileElement.remember('data');
      startTileData.pawnElement = undefined;
    }

    pawnData.startTileElement = tileElement;
    tileData.pawnElement = pawnElement;
  }

  placePawnElementInBox(pawnElement: SVG.Element) {
    const boxElement = this.boxElements.find(be => {
      const tileData: TileData = be.remember('data');
      return !tileData.pawnElement;
    });
    if (!boxElement) {
      return;
    }
    this.movePawnElementToElement(pawnElement, boxElement);
  }

  moveBox(duration: number) {
    if (this.boxContainerElement) {
      this.boxContainerElement.animate(duration, '<').translate(1000, 0).after(() => this.boxContainerElement.remove());
    }
  }
}
