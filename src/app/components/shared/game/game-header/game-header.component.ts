import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import GameState from '../../../../models/game/gamestate';
import Team from '../../../../models/game/team';
import GameRules from '../../../../models/game/gamerules';
import GamePlayer from '../../../../models/game/gameplayer';
import {animate, state, style, transition, trigger} from '@angular/animations';
import Action from '../../../../models/event/action';
import {GameService} from '../../../../services/game.service';
import ActionEvent from '../../../../models/event/action-event';
import GameTransitionToPlaying from '../../../../models/event/actions/game/game-transition-to-playing';

@Component({
  selector: 'app-game-header',
  templateUrl: './game-header.component.html',
  styleUrls: ['./game-header.component.scss'],
  animations: [
    trigger('prepState', [
      state('preparing', style({transform: 'translateY(0)', height: '75%', display: 'flex', opacity: '1'})),
      state('playing', style({transform: 'translateY(-100%)', height: '0', display: 'none', opacity: '0'})),
      transition('preparation => playing', animate('600ms ease-in'))
    ]),
    trigger('gameState', [
      state('preparing', style({transform: 'translateY(0)', height: '25%'})),
      state('playing', style({transform: 'translateY(0)', height: '100%'})),
      transition('preparation => playing', animate('600ms ease-in'))
    ]),
    trigger('timerState', [
      transition(':enter',
        [style({transform: 'scale(0)', opacity: '0', height: 0, overflow: 'hidden'}),
          animate('600ms', style({transform: 'scale(1)', opacity: '1', height: '*'}))]),
      transition(':leave',
        [style({height: '*', overflow: 'hidden'}),
          animate('600ms', style({transform: 'scale(0)', opacity: '0', height: 0}))])
    ])
  ],
})
export class GameHeaderComponent implements OnInit, OnDestroy {

  @Input() gameState: GameState;
  @Input() gameRules: GameRules;
  @Input() gamePlayers: GamePlayer[] = [];
  @Input() actionEventObservable: Observable<ActionEvent>;
  @Input() showTimer = false;

  animationState = 'preparation';

  private roundTimer: number;
  private interval;
  private actionEventsSub;
  private teamColors = {
    [Team.TEAM1]: 'blue',
    [Team.TEAM2]: 'red',
    [Team.TEAM3]: 'yellow',
    [Team.TEAM4]: 'green'
  };

  constructor(private gameService: GameService) {
  }

  ngOnInit() {
    if (this.actionEventObservable) {
      this.subscribeToActionEvents();
    }

    if (this.showTimer) {
      this.gameService.getRemainingTime().subscribe((timer: { time: number }) => {
        this.roundTimer = timer.time;
      });
    }
    if (this.gameState === GameState.PLAYING) {
      this.animationState = 'playing';
    }

    this.startTimer();
  }

  subscribeToActionEvents() {
    if (this.showTimer) {
      this.actionEventsSub = this.actionEventObservable.subscribe(actionEvent => {
          switch (actionEvent.action) {
            case Action.GAME_PLAYER_AFK:
            case Action.GAME_PAWN_MOVED:
            case Action.GAME_PAWN_ATTACKED:
            case Action.GAME_FLAG_KILLED:
              this.setRoundTimer();
              break;
            case Action.GAME_FINAL_BLOW:
            case Action.GAME_PASSIVE_WIN:
            case Action.GAME_HUMANS_ELIMINATED:
            case Action.GAME_ATTRITION_VICTORY:
              this.stopTimer();
              break;
            case Action.GAME_TRANSITION_TO_PLAYING:
              const payload = <GameTransitionToPlaying>actionEvent.payload;
              this.gamePlayers = payload.game.gamePlayers;
              this.gameState = payload.game.gameState;
              this.gameRules = payload.game.gameRules;
              this.setRoundTimer();
              this.animationState = 'playing';
              break;
          }
        }
      );
    }
  }

  ngOnDestroy() {
    if (this.showTimer) {
      this.stopTimer();
      this.actionEventsSub.unsubscribe();
    }
  }

  setRoundTimer() {
    const gamePlayer = this.gamePlayers.find(gp => gp.currentTurn);
    this.roundTimer = gamePlayer.afk ? this.gameRules.afkTimer : this.gameRules.turnTimer;
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.roundTimer--;
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.interval);
  }

  getRoundTime() {
    return this.roundTimer <= 0 ? 0 : this.roundTimer;
  }

  sortGamePlayers() {
    return this.gamePlayers.sort((gp1, gp2) => gp1.team.localeCompare(gp2.team));
  }

  isInPreparation() {
    return this.gameState === GameState.PREPARING;
  }

  getColor(team: Team
  ) {
    return this.teamColors[team];
  }

  isCurrentTurn(team: Team
  ) {
    return this.gamePlayers.find(gp => gp.team === team).currentTurn;
  }

  isReady(team: Team
  ) {
    return this.gamePlayers.find(gp => gp.team === team).ready;
  }

  isDead(team: Team
  ) {
    return this.gamePlayers.find(gp => gp.team === team).dead;
  }

  isAfk(team: Team) {
    return this.gamePlayers.find(gp => gp.team === team).afk;
  }
}
