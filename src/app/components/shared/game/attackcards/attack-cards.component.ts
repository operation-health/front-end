import {Component, Input, OnInit} from '@angular/core';
import Team from '../../../../models/game/team';
import Rank from '../../../../models/board/rank';
import ActionEvent from '../../../../models/event/action-event';
import {Observable} from 'rxjs';
import Action from '../../../../models/event/action';
import GamePawnAttacked from '../../../../models/event/actions/game/game-pawn-attacked';
import GameDoubleElimination from '../../../../models/event/actions/game/game-double-elimination';
import GameFlagKilled from '../../../../models/event/actions/game/game-flag-killed';
import GameAttritionKill from '../../../../models/event/actions/game/game-attrition-kill';
import GameAttritionVictory from '../../../../models/event/actions/game/game-attrition-victory';
import GameHumansEliminated from '../../../../models/event/actions/game/game-humans-eliminated';
import GameFinalBlow from '../../../../models/event/actions/game/game-final-blow';
import GamePassiveWin from '../../../../models/event/actions/game/game-passive-win';

interface CardPawn {
  rank: Rank;
  team: Team;
  dead: boolean;
  deadAnimation: boolean;
}

@Component({
  selector: 'app-attack-cards',
  templateUrl: './attack-cards.component.html',
  styleUrls: ['./attack-cards.component.scss']
})
export class AttackCardsComponent implements OnInit {

  @Input() actionEventObservable: Observable<ActionEvent>;

  attacker: CardPawn;
  defender: CardPawn;

  visible = false;
  revealed = false;

  private timeout;

  private teamColors = {
    [Team.TEAM1]: 'blue',
    [Team.TEAM2]: 'red',
    [Team.TEAM3]: 'yellow',
    [Team.TEAM4]: 'green'
  };

  ngOnInit() {
    if (this.actionEventObservable) {
      this.actionEventObservable.subscribe(actionEvent => {
        switch (actionEvent.action) {
          case Action.GAME_PAWN_ATTACKED: {
            const payload = <GamePawnAttacked>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_DOUBLE_ELIMINATION: {
            const payload = <GameDoubleElimination>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_FLAG_KILLED: {
            const payload = <GameFlagKilled>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_ATTRITION_KILL: {
            const payload = <GameAttritionKill>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_ATTRITION_VICTORY: {
            const payload = <GameAttritionVictory>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_HUMANS_ELIMINATED: {
            const payload = <GameHumansEliminated>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_FINAL_BLOW: {
            const payload = <GameFinalBlow>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
          case Action.GAME_PASSIVE_WIN: {
            const payload = <GamePassiveWin>actionEvent.payload;
            this.attacker = {
              rank: payload.movedPawnRank,
              team: payload.currentTeam,
              dead: payload.movedPawnDead,
              deadAnimation: false
            };
            this.defender = {
              rank: payload.attackedPawnRank,
              team: payload.attackedTeam,
              dead: payload.attackedPawnDead,
              deadAnimation: false
            };
            this.reveal();
            break;
          }
        }
      });
    }
  }

  reveal() {
    if (this.visible) {
      this.reset();
    }
    this.timeout = setTimeout(() => {
      this.visible = true;

      this.timeout = setTimeout(() => {
        this.revealed = true;

        this.timeout = setTimeout(() => {
          this.attacker.deadAnimation = this.attacker.dead;
          this.defender.deadAnimation = this.defender.dead;

          this.timeout = setTimeout(() => {
            this.reset();
          }, 2000);
        }, 600);
      }, 1000);
    }, 1);
  }

  reset() {
    this.visible = false;
    this.revealed = false;
    clearTimeout(this.timeout);
  }

  getBackImage(pawn: CardPawn) {
    const t = this.teamColors[pawn.team];
    return `assets/img/ranks/${t}/${t}-tower.svg`;
  }

  getPawnImage(pawn: CardPawn) {
    const t = this.teamColors[pawn.team];
    const r = pawn.rank.toString().toLowerCase();
    return `assets/img/ranks/${t}/${t}-${r}.svg`;
  }

}
