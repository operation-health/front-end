import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AttackCardsComponent} from './attack-cards.component';
import {CardComponent} from '../card/card.component';

describe('AttackCardsComponent', () => {
  let component: AttackCardsComponent;
  let fixture: ComponentFixture<AttackCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AttackCardsComponent,
        CardComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttackCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
