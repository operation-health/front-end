import {Component, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @ViewChild('flipContainer') private flipContainer;
  @Input() rightDirection: false;
  @Input() flip: boolean;

}
