import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Friend} from '../../../models/lobby/friend';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {PlayerService} from '../../../services/player.service';
import {LobbyStatsComponent} from '../lobby-stats/lobby-stats.component';
import {AddFriendComponent} from '../add-friend/add-friend.component';
import ActionEvent from '../../../models/event/action-event';
import {StreamService} from '../../../services/stream.service';
import Action from '../../../models/event/action';
import FriendRequest from '../../../models/event/actions/friend/friend-request';
import FriendRequestAccepted from '../../../models/event/actions/friend/friend-request-accepted';
import FriendRemoved from '../../../models/event/actions/friend/friend-removed';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit, OnDestroy {

  friends: Friend[] = [];
  recentlyPlayedPlayers: Friend[] = [];
  friendRequests: Friend[] = [];
  bsModalRef: BsModalRef;

  @Input()
  closeTab: () => void;

  private streamSub: Subscription;

  constructor(private playerService: PlayerService,
              private modalService: BsModalService,
              private streamService: StreamService) {
  }

  ngOnInit() {
    this.getFriends();
    this.getRecentlyPlayedPlayers();
    this.getFriendRequests();
    this.streamSub = this.streamService.getStream().subscribe((event: ActionEvent) => {
      switch (event.action) {
        case Action.FRIEND_REQUEST: {
          const payload = <FriendRequest>event.payload;
          this.friendRequests.push(payload);
          break;
        }
        case Action.FRIEND_REQUEST_ACCEPTED: {
          const payload = <FriendRequestAccepted>event.payload;
          this.friends.push(payload);
          this.friendRequests = this.friendRequests.filter(friend => friend.playerName !== payload.playerName);
          break;
        }
        case Action.FRIEND_REMOVED: {
          const payload = <FriendRemoved>event.payload;
          this.friends = this.friends.filter(friend => friend.playerName !== payload.playerName);
          break;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.streamSub.unsubscribe();
  }

  removeFriend(playerName: string) {
    this.playerService.removeFriend(playerName).subscribe();
  }

  addFriend(playerName: string) {
    this.playerService.addFriend(playerName).subscribe();
  }

  acceptFriendRequest(friend: Friend) {
    this.playerService.addFriend(friend.playerName).subscribe();
  }

  getFriends() {
    this.playerService.getFriends().subscribe((friends: any) => {
      this.friends = friends.friends;
    });
  }

  getFriendRequests() {
    this.playerService.getFriendRequests().subscribe((friendRequests: any) => {
      this.friendRequests = friendRequests;
    });
  }

  getRecentlyPlayedPlayers() {
    this.playerService.getRecentlyPlayed().subscribe((response: any) => {
      this.recentlyPlayedPlayers = response;
    });
  }

  openAddFriendModal() {
    this.bsModalRef = this.modalService.show(AddFriendComponent);
  }

  isFriend(playerName: string): boolean {
    return this.friends.map(friend => friend.playerName).includes(playerName);
  }

  openGamePlayerStatsModal(playerName: string) {
    const config = {
      initialState: {
        playerName
      },
      animated: true
    };
    this.bsModalRef = this.modalService.show(LobbyStatsComponent, config);
  }
}
