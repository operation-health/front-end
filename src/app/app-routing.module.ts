import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AuthGuard} from './guards/auth.guard';
import {LoginComponent} from './components/auth/login/login.component';
import {MainMenuComponent} from './components/main/main-menu/main-menu.component';
import {RulesComponent} from './components/main/rules/rules.component';
import {ConfirmEmailComponent} from './components/auth/confirm-email/confirm-email.component';
import {TabType} from './models/tab';
import {StatisticsComponent} from './components/main/statistics/statistics.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'main', component: MainMenuComponent, canActivate: [AuthGuard]},
  {path: 'replay', loadChildren: './components/replay/replay.module#ReplayModule'},
  {path: 'rules', component: RulesComponent, canActivate: [AuthGuard]},
  {path: 'profile', loadChildren: './components/profile/profile.module#ProfileModule'},
  {path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuard]},
  {path: 'lobby', loadChildren: './components/lobby/lobby.module#LobbyModule', data: {tabs: {CHAT_GAME: true}}},
  {path: 'game', loadChildren: './components/game/game.module#GameModule', data: {tabs: {CHAT_GAME: true}}},
  {path: 'api/authentication/confirmEmail/:token', component: ConfirmEmailComponent},
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})

export class AppRoutingModule {
}
