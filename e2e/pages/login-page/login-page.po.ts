import {browser, by, element, ElementArrayFinder, promise} from 'protractor';

export class LoginPage {

  navigateToLoginPage(): promise.Promise<any> {
    return browser.get('/login');
  }

  getTagWithClassTitle() {
    return element(by.className('title'));
  }

  getFacebookButton() {
    return element(by.buttonText('Facebook'));
  };

  getGoogleButton() {
    return element(by.buttonText('Google'));
  };


  getInputfields(): ElementArrayFinder {
    return element.all(by.tagName('input'));
  }

}
