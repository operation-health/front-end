import {LoginPage} from './login-page.po';
import {browser, by, element} from 'protractor';

describe('Login Page', () => {

  const loginPage = new LoginPage();

  beforeEach(() => {
    loginPage.navigateToLoginPage();
    browser.ignoreSynchronization = true;
  });

  it('Should find Google button', () => {
    expect(element(by.buttonText('Google')).isPresent());
  });

  it('Should find Facebook button', () => {
    expect(element(by.buttonText('Facebook')).isPresent());
  });


  it('Should redirect to the album page when learn more is clicked', () => {
    expect(loginPage.getTagWithClassTitle().getText()).toContain('Stratego');
  });

  it('Should open window with facebook.com when Google button is clicked', () => {
    const facebookButton = loginPage.getFacebookButton();
    facebookButton.click().then(function () {
      browser.getAllWindowHandles().then(function (handles) {
        const newWindowHandle = handles[1]; // this is your new window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('facebook.com');
          browser.switchTo().window(handles[0]);
        });
      });
    });
  });

  it('Should open window with accounts.google.com when Google button is clicked', () => {
    const googleButton = loginPage.getGoogleButton();
    googleButton.click().then(function () {
      browser.getAllWindowHandles().then(function (handles) {
        const newWindowHandle = handles[2]; // this is your new window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('accounts.google.com');
          browser.switchTo().window(handles[0]);
        });
      });
    });
  });

  it('Should have 2 inputs fields in the login page', () => {
    expect(loginPage.getInputfields().count()).toEqual(2);
  });
});

