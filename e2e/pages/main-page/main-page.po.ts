import {browser, by, element, ElementArrayFinder} from 'protractor';

export class MainPage {

  login() {
    browser.get('/login');
    browser.driver.findElement(by.id('register')).click();
    browser.driver.findElement(by.id('username')).sendKeys('Tester');
    browser.driver.findElement(by.id('email')).sendKeys('tester@tester.com');
    browser.driver.findElement(by.id('passwordRegister')).sendKeys('Password');
    browser.driver.findElement(by.id('submit')).click();
    browser.get('/login');
    browser.driver.findElement(by.id('name')).sendKeys('Tester');
    browser.driver.findElement(by.id('password')).sendKeys('Password');
    browser.driver.findElement(by.id('login')).click();
  }

  getButtonsInPage(): ElementArrayFinder {
    browser.sleep(1000);
    return element.all(by.tagName('button'));
  }

  logout() {
    browser.sleep(1000);
    browser.driver.findElement(by.id('logout')).click();
  }

}
