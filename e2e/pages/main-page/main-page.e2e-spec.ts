import {MainPage} from './main-page.po';
import {browser} from 'protractor';

describe('Main Page', () => {

  const mainPage = new MainPage();

  beforeEach(() => {
  });

  it('Should have 7 buttons on main page', () => {
    mainPage.login();
    browser.ignoreSynchronization = true;
    expect(mainPage.getButtonsInPage().count()).toEqual(7);
  });

  it('Should redirect to login page after log out', () => {
    mainPage.logout();
    browser.ignoreSynchronization = true;
    expect(browser.getCurrentUrl()).toContain('login');
  });
});

